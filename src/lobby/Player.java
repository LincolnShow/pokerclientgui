package lobby;

public class Player {
    private String name = null, balance = null, picPath = null;
    private int seat = -1;

    public Player(String name, String balance, String picPath) {
        this.name = name;
        this.balance = balance;
        this.picPath = picPath;
    }

    public Player(String name, String balance, String picPath, int seat) {
        this(name, balance, picPath);
        this.seat = seat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBalance() {
        return balance;
    }

    public int getIntBalance() {
        if (balance.length() >= 2) {
            return Integer.valueOf(balance.substring(1));
        }
        return -1;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }
}
