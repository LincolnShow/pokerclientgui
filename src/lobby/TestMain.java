package lobby;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import table.Constants;

import java.util.LinkedList;
import java.util.List;

class TestMain extends Application implements Constants {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Player pw = new Player("Joshua A. McGraham Jr.", "$100.000.000", "http://www.freakingnews.com/pictures/127000/Rowan-Atkinson-with-a-Small-Face--127153.jpg");
        ProfilePanel pp = new ProfilePanel(pw);
        pp.setMaxSize(pp.getWidth(), pp.getHeight());

        List<Player> plist = new LinkedList<>();
        plist.add(new Player("Joshua A. McGraham Jr.", "$100.000.000", "http://www.freakingnews.com/pictures/127000/Rowan-Atkinson-with-a-Small-Face--127153.jpg"));
        plist.add(new Player("Frisbey Jinkerson", "$100.000", "http://www.freakingnews.com/pictures/127000/Rowan-Atkinson-with-a-Small-Face--127153.jpg"));
        plist.add(new Player("Terley McCloud.", "$66.250", "http://www.freakingnews.com/pictures/127000/Rowan-Atkinson-with-a-Small-Face--127153.jpg"));
        plist.add(new Player("Some Poor Guy", "$100", "http://www.freakingnews.com/pictures/127000/Rowan-Atkinson-with-a-Small-Face--127153.jpg"));
        plist.add(new Player("Little", "$1", "http://www.freakingnews.com/pictures/127000/Rowan-Atkinson-with-a-Small-Face--127153.jpg"));
        plist.add(new Player("Player 6", "$13245568", "http://www.freakingnews.com/pictures/127000/Rowan-Atkinson-with-a-Small-Face--127153.jpg"));
        ObservableList<Player> pvs = FXCollections.observableList(plist);

        TableInfoPanel tipp = new TableInfoPanel();
        tipp.setMaxWidth(tipp.getWidth());

        GameModesPanel gmp = new GameModesPanel(new TexasHoldemMode());
        gmp.setMinHeight(50);

        List<Table> tList = new LinkedList<>();
        tList.add(new Table("Texas Hold'em 1", "$50/$100", "0/6"));
        tList.add(new Table("Texas Hold'em 2", "$100/$150", "5/6"));
        tList.add(new Table("Texas Hold'em 3", "$150/$200", "2/6"));
        tList.add(new Table("Texas Hold'em 4", "$200/$250", "4/6"));
        tList.add(new Table("Texas Hold'em 5", "$250/$300", "3/6"));
        tList.add(new Table("Texas Hold'em 6", "$300/$350", "1/6"));
        ObservableList<Table> tvs = FXCollections.observableList(tList);

        TableListPanel tlp = new TableListPanel(tvs);
        //tlp.setMaxWidth(tlp.getWidth());

        VBox vbMain = new VBox();
        vbMain.setAlignment(Pos.CENTER_RIGHT);
        vbMain.setSpacing(10d);
        vbMain.getChildren().addAll(pp, gmp, tlp, tipp);
        Scene scene = new Scene(vbMain);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.show();
        /*TableImageInteractivePanel tiip = new TableImageInteractivePanel("file:///D:\\IdeaProjects\\pokerclientgui\\out\\production\\PokerClientGUI\\com\\assets\\table.png", e->{
            Button b = (Button)e.getSource();
            int s = (int) b.getProperties().get("place");
            System.out.println("Place: " + s);
            System.out.println(e);
            b.setVisible(false);

        });
        HBox hb = new HBox(tiip);
        Scene scene = new Scene(hb);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.show();*/
    }
}
