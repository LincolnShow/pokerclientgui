package lobby;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import server.RequestHandler;
import server.Worker;
import server.requests.Answer;
import server.requests.LoginRequest;

import java.util.Optional;

public class GameClient extends Application {
    private static String balance;
    private static String picName = "playerLogo.jpg";
    private final static int loginW = 250;
    private final static int loginH = 200;

    public static String getBalance() {
        return balance;
    }

    public static void setBalance(String balance) {
        GameClient.balance = balance;
    }

    public static String getPicName() {
        return picName;
    }

    public static void setPicName(String picName) {
        GameClient.picName = picName;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Worker.primaryStage = primaryStage;
        Platform.setImplicitExit(false);
        Text tInfo = new Text();
        Label lLogin = new Label("Login:");
        lLogin.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        TextField tfLogin = new TextField("");
        tfLogin.setPromptText("Login");
        Label lPassword = new Label("Password:");
        lPassword.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        PasswordField pfPassword = new PasswordField();
        pfPassword.setPromptText("Password");
        Button bLogin = new Button("Log In");
        Button bSignUp = new Button("New User");
        Button bExit = new Button("Exit");
        bLogin.setScaleX(1.15);
        bLogin.setScaleY(1.15);
        bExit.setOnAction(e -> System.exit(0));

        HBox hbInfo = new HBox(0);
        hbInfo.setAlignment(Pos.CENTER);
        hbInfo.getChildren().add(tInfo);

        HBox hbLogin = new HBox(5);
        hbLogin.setAlignment(Pos.CENTER_RIGHT);
        hbLogin.getChildren().addAll(lLogin, tfLogin);

        HBox hbPassword = new HBox(5);
        hbPassword.setAlignment(Pos.CENTER_RIGHT);
        hbPassword.getChildren().addAll(lPassword, pfPassword);

        HBox hbButtons = new HBox(20);
        hbButtons.setAlignment(Pos.CENTER);
        hbButtons.getChildren().addAll(bSignUp, bLogin/*, bExit*/);


        GridPane gpMain = new GridPane();
        gpMain.setOnMousePressed(event -> gpMain.requestFocus());
        gpMain.getChildren().addAll(hbInfo, hbLogin, hbPassword, hbButtons);
        GridPane.setConstraints(hbInfo, 0, 0);
        GridPane.setConstraints(hbLogin, 0, 3);
        GridPane.setConstraints(hbPassword, 0, 4);
        GridPane.setConstraints(hbButtons, 0, 12);
        gpMain.setVgap(5d);
        gpMain.setAlignment(Pos.CENTER);
        Scene loginScreenScene = new Scene(gpMain, loginW, loginH);
        loginScreenScene.getStylesheets().add(getClass().getResource("loginDialog.css").toExternalForm());
        gpMain.requestFocus();
        primaryStage.setTitle("Sign In");
        primaryStage.setScene(loginScreenScene);
        primaryStage.setResizable(false);
        Worker.tfLobbyLogin = tfLogin;
        Worker.pfLobbyPassword = pfPassword;
        /*Task task = new Task() {
            @Override
            protected Object call() throws Exception {
                Worker.startListening();
                return null;
            }
        };
        Thread t = new Thread(task);
        t.setDaemon(true);
        t.start();*/
        Worker.startListening();
        bSignUp.setOnAction(event -> {
            Optional<String> s = new RegistrationDialog().showAndWait();
            if (s.isPresent()) {
                tInfo.setStyle("-fx-fill: green");
                tInfo.setText(s.get());
            }
        });
        bLogin.setOnAction(event -> {
            Answer a = new Answer(false, "null");
            if (tfLogin.textProperty().isEmpty().get() ||
                    pfPassword.textProperty().isEmpty().get() /*|| !(a = RequestHandler.send(new LoginRequest(tfLogin.getText(), pfPassword.getText())).isSuccess*/) {
                tInfo.setStyle("-fx-fill: red");
                tInfo.setText("Invalid credentials");
            } else {
                RequestHandler.send(new LoginRequest(tfLogin.getText(), pfPassword.getText()));
            }
        });
        tfLogin.textProperty().addListener(observable -> {
            tInfo.setText(null);
        });
        primaryStage.setOnCloseRequest(e -> {
            System.exit(0);
        });
        primaryStage.show();
    }
}