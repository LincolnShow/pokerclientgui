package lobby;

import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class TablePlayersInfoPanel extends VBox {
    private TableView playersTable = new TableView();

    public TablePlayersInfoPanel() {
        super();
        TableColumn nameColumn = new TableColumn("Name");
        TableColumn balanceColumn = new TableColumn("Balance");
        nameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("name"));
        balanceColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("balance"));
        playersTable.getColumns().addAll(nameColumn, balanceColumn);
        playersTable.setEditable(false);
        playersTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        getChildren().add(this.playersTable);
        Label l = new Label("The table is empty");
        l.setFont(Font.font("Arial", 26d));
        l.setStyle("-fx-text-fill: gray");
        playersTable.setPlaceholder(l);
    }

    public TablePlayersInfoPanel(ObservableList<Player> tablePlayersList) {
        this();
        setTablePlayers(tablePlayersList);
        setTablePlayers(tablePlayersList);
        setSpacing(5);
    }

    public final void setTablePlayers(ObservableList<Player> tablePlayersViews) {
        playersTable.setItems(tablePlayersViews);
    }

    public TableView getPlayersTable() {
        return playersTable;
    }
}
