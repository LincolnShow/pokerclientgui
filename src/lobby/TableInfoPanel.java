package lobby;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import server.RequestHandler;
import server.requests.TakeSeatRequest;

import java.util.Optional;

public class TableInfoPanel extends VBox {
    private TablePlayersInfoPanel playersInfo = new TablePlayersInfoPanel();
    private Label tableName = new Label();
    private TableImageInteractivePanel tableImageInteractivePanel = null;
    private Label tableStakes = new Label();

    public TableInfoPanel() {
        super();
        this.tableName.setStyle("-fx-font-size: 26; -fx-text-fill: orange; -fx-font-weight: bold;");
        this.tableStakes.setStyle("-fx-text-fill: rgba(25, 255, 25, 0.7); -fx-font-weight: bolder; -fx-font-size: 22;");
        setTableImageInteractivePanel(new TableImageInteractivePanel(getClass().getResource("table.png").toExternalForm(), e -> {
            Button b = (Button) e.getSource();
            int place = (int) b.getProperties().get("place");
            System.out.println("Place: " + place);
            System.out.println(e);
            //Request for player's chips
            //int playerChips = ...;
            String[] split = tableStakes.getText().split("/");
            Optional<String> r = new TableSitDialog(ProfilePanel.getBalance(), Integer.parseInt(tableStakes.getText().substring(tableStakes.getText().indexOf('/') + 2))).showAndWait();
            if (r.isPresent()) {
                //Sending a request to the server
                System.out.println(r.get());
                RequestHandler.send(new TakeSeatRequest(tableName.getText(), String.valueOf(place), r.get()));
            }
        }));
        setAlignment(Pos.CENTER);
        getChildren().addAll(this.tableName, getTableImageInteractivePanel(), this.tableStakes, getPlayersInfo());
        setStyle("-fx-border-color: black; -fx-border-radius: 5px; ");
        getStylesheets().add(getClass().getResource("tableInfoPanel.css").toExternalForm());
        setSpacing(5d);
        getTableImageInteractivePanel().setVisible(false);
    }

    public void loadTableData(Table table) {
        tableName.setText(table.getName());
        tableStakes.setText(table.getStakes());
        if (table.getPlayers() != null && !table.getPlayers().isEmpty()) {
            getPlayersInfo().setTablePlayers(table.getPlayers());
        }
        getTableImageInteractivePanel().setVisible(true);
    }

    public String getTableName() {
        return tableName.getText();
    }

    public int getSB() {
        String s = tableStakes.getText();
        return Integer.valueOf(s.substring(s.indexOf("$") + 1, s.indexOf("/")));
    }

    public int getBB() {
        String s = tableStakes.getText();
        return Integer.valueOf(s.substring(s.indexOf("/") + 2));
    }

    public TablePlayersInfoPanel getPlayersInfo() {
        return playersInfo;
    }

    public void setPlayersInfo(TablePlayersInfoPanel playersInfo) {
        this.playersInfo = playersInfo;
    }

    public TableImageInteractivePanel getTableImageInteractivePanel() {
        return tableImageInteractivePanel;
    }

    public void setTableImageInteractivePanel(TableImageInteractivePanel tableImageInteractivePanel) {
        this.tableImageInteractivePanel = tableImageInteractivePanel;
    }
}