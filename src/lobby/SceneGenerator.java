package lobby;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import server.RequestHandler;
import server.Worker;
import server.TableInfoUpdater;
import server.requests.GetTableInfoRequest;
import server.requests.GetTablesRequest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class SceneGenerator {
    public static Scene buildLobbyFor(Player player) {
        VBox vbLeft = new VBox(), vbRight = new VBox();
        UpperLogoPanel ulp = new UpperLogoPanel();
        GamesPanel gp = new GamesPanel();
        ProfilePanel pp = new ProfilePanel(player);
        TableInfoPanel tip = new TableInfoPanel();
        ulp.setLogo(new Image(SceneGenerator.class.getResource("logo.png").toExternalForm()));

        vbLeft.getChildren().addAll(ulp,
                gp);
        vbRight.getChildren().addAll(pp,
                tip);
        new Thread(() -> {
            while (true) {
                if (Worker.updateLobby) {
                    RequestHandler.send(new GetTablesRequest(pp.getName()));
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        gp.getTableListPanel().getTables().getSelectionModel().selectedItemProperty().addListener((obs, oldS, newS) -> {
            //TO DO
            //Properly update TableInfoPanel
            if (newS != null && (newS != oldS)) {
                if (gp.getTableInfoUpdater() != null) {
                    gp.getTableInfoUpdater().stop();
                    gp.setTableInfoUpdater(null);
                }
                tip.getPlayersInfo().setTablePlayers(null);
                tip.getTableImageInteractivePanel().disableAllButtons();
                Table t = (Table) newS;
                RequestHandler.send(new GetTableInfoRequest(t.getName()));
                gp.setTableInfoUpdater(new TableInfoUpdater(1000, t, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (Worker.updateLobby) {
                            RequestHandler.send(new GetTableInfoRequest(t.getName()));
                        }
                    }
                }));
                Platform.runLater(() -> {
                    tip.loadTableData(gp.getTableInfoUpdater().getTable());
                });
                gp.getTableInfoUpdater().start();
            }
        });
        vbLeft.setSpacing(50d);
        vbRight.setSpacing(10d);
        VBox.setVgrow(gp.getTableListPanel(), Priority.ALWAYS);
        BorderPane mainContainer = new BorderPane();
        mainContainer.setLeft(vbLeft);
        mainContainer.setRight(vbRight);
        mainContainer.getStylesheets().add(GameClient.class.getResource("lobby.css").toExternalForm());
        ulp.setMaxWidth(ulp.getWidth());
        gp.setPrefWidth(500);
        Worker.gatherPanels(gp, ulp, pp, tip);
        return new Scene(mainContainer, 825, 600);
    }
}
