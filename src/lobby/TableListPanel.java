package lobby;

import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;


public class TableListPanel extends VBox {
    private TableView tables = new TableView();
    private ObservableList<Table> tablesList;

    public TableListPanel(ObservableList<Table> tables) {
        this();
        setTables(tables);
    }

    public TableListPanel() {
        super();
        TableColumn nameColumn = new TableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<Table, String>("name"));
        TableColumn stakesColumn = new TableColumn("Stakes");
        stakesColumn.setCellValueFactory(new PropertyValueFactory<Table, String>("stakes"));
        TableColumn playersColumn = new TableColumn("Players");
        playersColumn.setCellValueFactory(new PropertyValueFactory<Table, String>("playersInfo"));
        tables.setEditable(false);
        tables.getColumns().addAll(nameColumn, stakesColumn, playersColumn);
        tables.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        getChildren().add(tables);
        getStylesheets().add(getClass().getResource("tableListPanel.css").toExternalForm());
    }

    public final void setTables(ObservableList<Table> tables) {
        tablesList = tables;
        this.tables.setItems(tables);
    }

    public TableView getTables() {
        return tables;
    }

    public ObservableList<Table> getTablesList() {
        return tablesList;
    }

    public void setTablesList(ObservableList<Table> tablesList) {
        this.tablesList = tablesList;
    }
}
