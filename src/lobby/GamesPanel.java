package lobby;

import javafx.scene.layout.VBox;
import server.TableInfoUpdater;

public class GamesPanel extends VBox {
    private GameModesPanel gameModesPanel = null;
    private TableListPanel tableListPanel = null;
    private TableInfoUpdater tableInfoUpdater = null;

    public GamesPanel() {
        this(new GameModesPanel(new TexasHoldemMode()), new TableListPanel());
    }

    private GamesPanel(GameModesPanel gameModesPanel, TableListPanel tableListPanel) {
        super();
        this.gameModesPanel = gameModesPanel;
        this.tableListPanel = tableListPanel;
        getChildren().addAll(gameModesPanel, tableListPanel);
        setSpacing(5d);
    }

    public GameModesPanel getGameModesPanel() {
        return gameModesPanel;
    }

    public void setGameModesPanel(GameModesPanel gameModesPanel) {
        this.gameModesPanel = gameModesPanel;
    }

    public TableListPanel getTableListPanel() {
        return tableListPanel;
    }

    public void setTableListPanel(TableListPanel tableListPanel) {
        this.tableListPanel = tableListPanel;
    }

    public TableInfoUpdater getTableInfoUpdater() {
        return tableInfoUpdater;
    }

    public void setTableInfoUpdater(TableInfoUpdater tableInfoUpdater) {
        this.tableInfoUpdater = tableInfoUpdater;
    }
}
