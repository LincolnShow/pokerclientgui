package lobby;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;

class RegistrationDialog extends TextInputDialog {
    public RegistrationDialog() {
        super();
        getDialogPane().getStylesheets().add(getClass().getResource("newUserDialog.css").toExternalForm());
        initModality(Modality.APPLICATION_MODAL);
        setTitle("Sign Up");
        setHeaderText("New user registration");
        setGraphic(null);
        VBox vbPane = new VBox(15);
        Text tInfo = new Text();
        tInfo.setStyle("-fx-fill: red");
        TextField tfLogin = new TextField();
        PasswordField tfPassword = new PasswordField(), tfPasswordRe = new PasswordField();
        tfLogin.setPromptText("Login");
        tfPassword.setPromptText("Password");
        tfPasswordRe.setPromptText("Repeat password");

        vbPane.getChildren().addAll(tfLogin, tfPassword, tfPasswordRe, tInfo);
        vbPane.setAlignment(Pos.CENTER);
        ButtonType newUserButtonType = new ButtonType("Register", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(newUserButtonType);
        getDialogPane().getButtonTypes().remove(ButtonType.OK);
        getDialogPane().setContent(vbPane);
        vbPane.setOnMousePressed(e -> vbPane.requestFocus());
        Node newUserButton = getDialogPane().lookupButton(newUserButtonType);

        ChangeListener<String> stringChangeListener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (tfLogin.textProperty().isNotEmpty().get() &&
                        tfPassword.textProperty().isNotEmpty().get() &&
                        tfPassword.textProperty().get().equals(tfPasswordRe.textProperty().get())) {
                    tInfo.setText(null);
                    newUserButton.setDisable(false);
                } else {
                    if (!tfPassword.textProperty().get().equals(tfPasswordRe.textProperty().get())) {
                        tInfo.setText("Passwords must match");
                    }
                    newUserButton.setDisable(true);
                }
            }
        };
        tfLogin.textProperty().addListener(stringChangeListener);
        tfPassword.textProperty().addListener(stringChangeListener);
        tfPasswordRe.textProperty().addListener(stringChangeListener);

        // Convert the result when the login button is clicked.
        setResultConverter(dialogButton -> {
            if (dialogButton == newUserButtonType) {
                return "Registration successful";
            }
            return null;
        });

        newUserButton.setDisable(true);
        vbPane.requestFocus();
    }

}
