package lobby;

import javafx.collections.ObservableList;

public class Table {
    private String name, stakes, playersInfo;
    private ObservableList<Player> players = null;

    public Table(String name, String stakes, String playersInfo) {
        this.name = name;
        this.stakes = stakes;
        this.playersInfo = playersInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStakes() {
        return stakes;
    }

    public void setStakes(String stakes) {
        this.stakes = stakes;
    }

    public String getPlayersInfo() {
        return playersInfo;
    }

    public void setPlayersInfo(String playersInfo) {
        this.playersInfo = playersInfo;
    }

    public ObservableList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ObservableList<Player> players) {
        this.players = players;
    }
}
