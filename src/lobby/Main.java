package lobby;

import javafx.application.Application;

class Main {
    public static void main(String[] args) {
        Application.launch(GameClient.class, args);
    }
}
