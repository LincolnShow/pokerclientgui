package lobby;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.File;
import java.net.URI;

public class ProfilePanel extends HBox {
    private static HBox profilePic = new HBox();
    private static HBox name = new HBox();
    private static HBox balance = new HBox();
    private static GridPane mainC = new GridPane();

    public ProfilePanel(Player player) {
        super();
        Text nameText = new Text(player.getName()), balanceText = new Text(player.getBalance());
        name.getChildren().add(nameText);
        name.setMinWidth(125);
        balance.getChildren().add(balanceText);
        ImageView iw = new ImageView();
        iw.setSmooth(true);
        iw.setImage(new Image(getClass().getResource(player.getPicPath()).toExternalForm()));
        iw.setPreserveRatio(false);
        iw.setFitWidth(60);
        iw.setFitHeight(60);
        profilePic.setFillHeight(true);
        profilePic.getChildren().add(iw);
        mainC.add(profilePic, 0, 0);
        mainC.add(name, 1, 0);
        mainC.add(balance, 0, 1, 2, 1);
        nameText.setFont(Font.font("Arial", 18d));
        balanceText.setFont(Font.font("Arial", 26d));
        name.setAlignment(Pos.CENTER);
        balance.setAlignment(Pos.CENTER);
        setAlignment(Pos.TOP_RIGHT);
        mainC.setAlignment(Pos.CENTER);
        mainC.setHgap(3d);
        mainC.setVgap(5d);
        balanceText.setStyle("-fx-fill: darkgreen; -fx-font-weight: bold;");
        nameText.setStyle("-fx-fill: white;");
        mainC.setStyle("-fx-border-width: 2px; " +
                "-fx-border-radius: 4px; " +
                "-fx-border-color: #BBB9AA; " +
                "-fx-background-color: linear-gradient(#424141 0%, #000000 100%);");
        setSpacing(10d);
        Button bCashier = new Button("Cashier");
        //TO DO
        //Proper handler
        bCashier.setOnAction(e -> {
            System.out.println("You are out of money.");
        });
        bCashier.setPrefHeight(60);
        setAlignment(Pos.CENTER);
        getChildren().add(bCashier);
        getChildren().add(mainC);
        getStylesheets().add(getClass().getResource("profilePanel.css").toExternalForm());
        profilePic.setStyle("-fx-border-color: gray; -fx-border-radius: 5px; -fx-border-width: 2px;");

    }

    public String getName() {
        return ((Text) name.getChildren().get(0)).getText();
    }

    public static int getBalance() {
        return Integer.valueOf(((Text) balance.getChildren().get(0)).getText().substring(1));
    }
    public void setBalance(int newBalance){
        ((Text)balance.getChildren().get(0)).setText(String.format("$%d", newBalance));
    }
}
