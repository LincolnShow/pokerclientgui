package lobby;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;

class TableSitDialog extends TextInputDialog {
    public TableSitDialog(int max, int min) {
        super();
        initModality(Modality.APPLICATION_MODAL);
        getDialogPane().getStylesheets().add(getClass().getResource("tableSitDialog.css").toExternalForm());
        setGraphic(new ImageView(new Image(getClass().getResource("chips.png").toExternalForm())));
        setTitle("Buy-In");
        setHeaderText("Buy-In");
        VBox vbMain = new VBox(50);
        HBox hbAvailable = new HBox(5d);
        Label lAvailable = new Label("Available:");
        Label lAvailableNumber = new Label("$" + String.valueOf(max));
        lAvailableNumber.setStyle("-fx-text-fill: darkgreen; -fx-font-size: 22; -fx-font-weight: bolder;");
        hbAvailable.getChildren().addAll(lAvailable, lAvailableNumber);
        lAvailable.setStyle("-fx-font-size: 16; -fx-font-weight: bold");
        VBox vbBottom = new VBox(10);
        HBox hbBuyIn = new HBox(10d);
        Label lBuyIn = new Label("Buy-In Amount:");
        lBuyIn.setStyle("-fx-text-fill: black; -fx-font-size: 16;");
        TextField tfBuyIn = new TextField(String.valueOf(max));
        tfBuyIn.setMaxWidth(tfBuyIn.getFont().getSize() * String.valueOf(max).length());
        tfBuyIn.setAlignment(Pos.CENTER);
        tfBuyIn.setStyle("-fx-font-size: 14; -fx-font-weight: bold; ");
        hbBuyIn.getChildren().addAll(lBuyIn, tfBuyIn);
        ScrollBar sbBuyIn = new ScrollBar();
        sbBuyIn.setMaxWidth(200);
        sbBuyIn.setMax(max);
        sbBuyIn.setMin(min);
        sbBuyIn.setValue(max);
        sbBuyIn.setMinHeight(20d);
        sbBuyIn.setStyle("-fx-fill: blue;");
        sbBuyIn.valueProperty().addListener(e -> {
            tfBuyIn.setText(String.valueOf((int) sbBuyIn.getValue()));
        });
        tfBuyIn.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.equals("") && newValue.matches("\\d*")) {
                    if (Integer.parseInt(newValue) >= max) {
                        tfBuyIn.setText(String.valueOf(max));
                    } else if (Integer.parseInt(newValue) < min) {
                        tfBuyIn.setText(String.valueOf(min));
                    }
                    sbBuyIn.setValue(Double.parseDouble(tfBuyIn.getText()));
                } else {
                    tfBuyIn.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        vbBottom.getChildren().addAll(hbBuyIn, sbBuyIn);
        hbAvailable.setAlignment(Pos.CENTER);
        vbMain.getChildren().addAll(hbAvailable, vbBottom);
        hbAvailable.setStyle("-fx-border-width: 2px; -fx-border-color: gray; -fx-border-radius: 5px;");
        vbMain.setStyle("-fx-background-color: lightgray");
        getDialogPane().setStyle("-fx-background-color: darkgray;");
        vbBottom.setAlignment(Pos.CENTER);
        vbMain.setAlignment(Pos.CENTER);
        getDialogPane().setContent(vbMain);
        setResultConverter(button -> {
            if (button == ButtonType.OK) {
                return tfBuyIn.getText();
            }
            return null;
        });
    }
}
