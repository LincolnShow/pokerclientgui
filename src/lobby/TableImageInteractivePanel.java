package lobby;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;

import java.util.LinkedList;
import java.util.List;

public class TableImageInteractivePanel extends StackPane {
    private ImageView table = null;
    private List<Button> sitButtons = new LinkedList<>();

    public TableImageInteractivePanel(String imgPath, EventHandler<ActionEvent> eventHandler) {
        this.table = new ImageView(new Image(imgPath));
        table.setFitWidth(300);
        table.setPreserveRatio(true);
        getChildren().add(table);

        Bounds bounds = table.getBoundsInLocal();
        for (int i = 0; i < 6; ++i) {
            getSitButtons().add(new Button());
            Button b = getSitButtons().get(i);
            b.setShape(new Circle(1));
            b.setMinSize(30, 30);
            b.getProperties().put("place", i);
            b.setOnAction(eventHandler);
            getChildren().add(b);
        }
        getSitButtons().get(0).setTranslateX(-bounds.getWidth() / 6);
        getSitButtons().get(0).setTranslateY(-bounds.getHeight() / 3);

        getSitButtons().get(1).setTranslateX(+bounds.getWidth() / 6);
        getSitButtons().get(1).setTranslateY(-bounds.getHeight() / 3);

        getSitButtons().get(2).setTranslateX(+bounds.getWidth() / 2.3);

        getSitButtons().get(3).setTranslateX(+bounds.getWidth() / 6);
        getSitButtons().get(3).setTranslateY(+bounds.getHeight() / 3);

        getSitButtons().get(4).setTranslateX(-bounds.getWidth() / 6);
        getSitButtons().get(4).setTranslateY(+bounds.getHeight() / 3);

        getSitButtons().get(5).setTranslateX(-bounds.getWidth() / 2.3);
        getStylesheets().add(getClass().getResource("tableImageInteractive.css").toExternalForm());
        /*int i = 0;
        for (Button b : sitButtons){
            b.setShape(new Circle(60));
            b.setStyle("-fx-border-color: green;"+
                        "-fx-border-width: 2px;" +
                        "-fx-background-color: darkgreen;");
            b.setMinSize(30,30);
            b.getProperties().put("place", i++);
            b.setOnAction(eventHandler);
            getChildren().add(b);
        }*/
    }

    public void enableAllButtons() {
        for (Button b : getSitButtons()) {
            b.setDisable(false);
            b.setText("");
        }
    }

    public void disableAllButtons() {
        for (Button b : getSitButtons()) {
            b.setDisable(true);
        }
    }

    public void disableButtons(int... indexes) {
        for (int i : indexes) {
            //sitButtons.get(i).setVisible(false);
            getSitButtons().get(i).setDisable(true);
        }
    }

    public void enableButtons(int... indexes) {
        for (int i : indexes) {
            //sitButtons.get(i).setVisible(true);
            getSitButtons().get(i).setDisable(false);
        }
    }

    public List<Button> getSitButtons() {
        return sitButtons;
    }

    public void setSitButtons(List<Button> sitButtons) {
        this.sitButtons = sitButtons;
    }
}
