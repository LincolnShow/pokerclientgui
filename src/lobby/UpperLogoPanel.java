package lobby;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class UpperLogoPanel extends HBox {
    private ImageView logo = new ImageView();

    public UpperLogoPanel() {
        super();
        setStyle("-fx-border-color: black;\n" +
                "    -fx-border-radius: 4px;\n" +
                "    -fx-border-width: 2px;");
        getChildren().add(logo);
    }

    public UpperLogoPanel(Image i) {
        this();
        setLogo(i);
    }

    public final void setLogo(Image i) {
        logo.setImage(i);
        logo.setPreserveRatio(false);
        logo.setFitWidth(300);
        logo.setFitHeight(100);
    }
}
