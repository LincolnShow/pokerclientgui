package lobby;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class GameModesPanel extends HBox {
    public GameModesPanel(GameMode... modes) {
        super();
        for (GameMode m : modes) {
            Button b = new Button(m.toString());
            //b.setShape(new Circle(5));
            b.setPrefHeight(50);
            setHgrow(b, Priority.ALWAYS);
            getChildren().add(b);
            b.setOnAction(e -> {
                //TO DO
                //Sending the server a request for a table list with the specific game mode
            });
            getStylesheets().add(getClass().getResource("gameModesPanel.css").toExternalForm());
            setAlignment(Pos.CENTER);
        }
    }
}
