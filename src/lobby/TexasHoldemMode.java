package lobby;

class TexasHoldemMode implements GameMode {
    private Mode m = Mode.TEXAS_HOLDEM;

    @Override
    public String toString() {
        return "Texas Hold'em";
    }
}
