package server;

import lobby.Table;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TableInfoUpdater extends Timer {
    private Table table;

    public TableInfoUpdater(int delay, Table table, ActionListener listener) {
        super(delay, listener);
        this.table = table;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
