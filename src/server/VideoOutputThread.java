package server;

import com.github.sarxos.webcam.Webcam;
import table.Constants;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;

public class VideoOutputThread extends Thread implements Constants {
    private volatile int broadcastPort = 0;
    private volatile boolean isRunning = false;
    private Webcam webcam = null;
    private DatagramSocket sender;

    public VideoOutputThread(Webcam w, int broadcastPort) {
        webcam = w;
        this.broadcastPort = broadcastPort;
        try {
            sender = new DatagramSocket();
            sender.setBroadcast(true);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public int getBroadcastPort() {
        return broadcastPort;
    }

    public void setBroadcastPort(int broadcastPort) {
        this.broadcastPort = broadcastPort;
    }

    @Override
    public void run() {
        setName("VideoOutputThread");
        isRunning = true;
        BufferedImage img;
        ByteArrayOutputStream baos;
        DatagramPacket sendPacket;
        byte[] sendData;
        while (isRunning) {
            try {
                img = webcam.getImage();
                baos = new ByteArrayOutputStream();
                ImageIO.write(img, "jpg", baos);
                sendData = baos.toByteArray();
                sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("255.255.255.255"), broadcastPort);
                sender.send(sendPacket);
                Thread.sleep(16);
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    @Override
    public void interrupt() {
        isRunning = false;
    }

    @Override
    public boolean isInterrupted() {
        return !isRunning;
    }
}
