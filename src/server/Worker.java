package server;

import com.github.sarxos.webcam.Webcam;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lobby.*;
import server.requests.*;
import table.*;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public abstract class Worker implements Constants {
    private static boolean DEBUG = false;
    private static Thread t = null;
    public static Stage primaryStage;
    public static TextField tfLobbyLogin;
    public static PasswordField pfLobbyPassword;
    public static GamesPanel gp;
    public static UpperLogoPanel ulp;
    public static ProfilePanel pp;
    public static TableInfoPanel tip;
    public static Socket server;
    public static boolean isListening = false;
    public static volatile boolean isFree = true;
    public static volatile boolean isLogged = false;
    public static volatile boolean isSitting = false;
    public static volatile boolean updateLobby = true;
    public static volatile PokerTableWindow pGame = null;

    private static int thisPlayerSeat = -1;
    private static volatile Webcam webcam = null;
    public static VideoOutputThread vOt;
    public static VideoInputThread vIt;
    public static volatile int[] activePorts = new int[6];
    private static int potUpdate = 0;

    static {
        try {
            InputStream in = Worker.class.getResourceAsStream("server.info");
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String s;
            s = br.readLine();
            server = new Socket(s, 3333);
            s = br.readLine();
            try {
                DEBUG = Boolean.parseBoolean(s.split("=")[1]);
            }catch (Exception ignored){}
            in.close();
            br.close();
            /*new Thread(()->{
                while(true){
                    RequestHandler.sendRaw((byte)1);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();*/
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            Platform.exit();
            System.exit(2);
        }
    }

    public static void gatherPanels(GamesPanel gp, UpperLogoPanel ulp, ProfilePanel pp, TableInfoPanel tip) {
        Worker.gp = gp;
        Worker.ulp = ulp;
        Worker.pp = pp;
        Worker.tip = tip;
    }

    public static void startListening() {
        try {
            server.setSoTimeout(5);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        if (!isListening) {
            Worker.isListening = true;
            t = new Thread(() -> {
                Platform.setImplicitExit(false);
                List<Table> tList = new ArrayList<>();
                ObservableList<Table> tvs;
                List<Player> activePlayers = new ArrayList<>();
                ObservableList<Player> active = null;
                byte[] answer;
                String sAnswer;
                Answer a;
                int tablesLeft = 0;
                int playersLeft = 0;
                while (true) {
                    answer = new byte[256];
                    int i = 0;
                        try {
                            try {
                                int r = 0;
                                int counter = 5;
                                do {
                                    r = server.getInputStream().read();
                                    if ((char) r == ';') {
                                        --counter;
                                    }
                                    if (r == -1 || counter == 0) {
                                        break;
                                    }
                                    answer[i++] = (byte) r;
                                } while (server.getInputStream().available() > 0);
                            } catch (SocketTimeoutException ignored){
                            } catch (SocketException e){
                                System.out.println(e.getLocalizedMessage());
                                Platform.exit();
                                System.exit(2);
                            }
                            sAnswer = new String(answer).trim();
                            String[] sAnswers = null;
                            if (!sAnswer.isEmpty()) {
                                isFree = false;
                                sAnswers = sAnswer.split(";");
                                System.out.println("{");
                                for (String s : sAnswers) {
                                    System.out.println(s);
                                    a = new Answer(true, s);
                                    if (s.contains("login")) {
                                        if (LoginRequest.checkSuccess(s)) {
                                            GameClient.setBalance("$" + String.valueOf(LoginRequest.parseBalance(a)));
                                            isLogged = true;
                                            Platform.runLater(() -> {
                                                primaryStage.hide();
                                                primaryStage.setScene(SceneGenerator.buildLobbyFor(new Player(tfLobbyLogin.getText(), GameClient.getBalance(), GameClient.getPicName())));
                                                primaryStage.setTitle("Game Lobby");
                                                primaryStage.show();
                                            });
                                        }
                                        isFree = true;
                                    } else if (s.contains("tablecount")) {
                                        if (tablesLeft == 0) {
                                            tablesLeft = Integer.valueOf(s.substring(s.indexOf("=") + 1, s.lastIndexOf("=")));
                                            tList.clear();
                                        }
                                    } else if (s.contains("table=")) {
                                        --tablesLeft;
                                        tList.add(GetTablesRequest.parseTable(s));
                                        if (tablesLeft == 0) {
                                            tvs = FXCollections.observableArrayList(tList);
                                            //int pos = gp.getTableListPanel().getTables().getSelectionModel().getFocusedIndex();
                                            gp.getTableListPanel().setTables(tvs);

                                            //gp.getTableListPanel().getTables().getSelectionModel().focus(pos);
                                            //gp.getTableListPanel().getTables().getSelectionModel().clearAndSelect(pos);
                                            isFree = true;
                                        }
                                    } else if (s.contains("totalPlayers")) {
                                        playersLeft = Integer.valueOf(s.substring(s.indexOf("=") + 1, s.lastIndexOf("=")));
                                        if (playersLeft != 0) {
                                            activePlayers.clear();
                                        } else {
                                            Platform.runLater(() -> {
                                                tip.getPlayersInfo().setTablePlayers(null);
                                                tip.getTableImageInteractivePanel().enableAllButtons();
                                            });
                                            activePlayers.clear();
                                            isFree = true;
                                        }
                                    } else if (s.contains("active")) {
                                        --playersLeft;
                                        Player p = GetTableInfoRequest.parsePlayer(s);
                                        activePlayers.add(p);
                                        if (playersLeft == 0) {
                                            //gp.tableInfoUpdater.getTable().setPlayers(active);
                                            if (gp.getTableInfoUpdater().getTable().getName().equals(tip.getTableName())) {
                                                active = FXCollections.observableList(activePlayers);
                                                tip.getPlayersInfo().setTablePlayers(active);
                                                //tip.tableImageInteractivePanel.enableAllButtons();
                                                Platform.runLater(() -> {
                                                    tip.getTableImageInteractivePanel().enableAllButtons();
                                                    for (Player player : activePlayers) {
                                                        tip.getTableImageInteractivePanel().disableButtons(player.getSeat());
                                                        tip.getTableImageInteractivePanel().getSitButtons().get(player.getSeat()).setText(player.getName());
                                                    }
                                                });
                                            }
                                            //final String sf = String.format("%d/6", activePlayers.size());
                                        /*Platform.runLater(() -> {
                                            tip.loadTableData(gp.tableInfoUpdater.getTable());
                                            int index = gp.getTableListPanel().getTablesList().indexOf(gp.tableInfoUpdater.getTable());
                                            gp.getTableListPanel().getTablesList().get(index).setPlayersInfo(sf);
                                        });*/
                                            isFree = true;
                                        }

                                    } else if (s.contains("takesseat")) {
                                        if (TakeSeatRequest.checkSuccess(s)) {
                                            if (!isSitting && TakeSeatRequest.parsePlayerName(s).equals(pp.getName())) {
                                                if(!DEBUG) {
                                                    if (webcam == null) {
                                                        webcam = Webcam.getDefault();
                                                        webcam.setViewSize(new Dimension(176, 144));
                                                    }
                                                    vIt = new VideoInputThread();
                                                    vIt.setPriority(Thread.MIN_PRIORITY);
                                                    vIt.setDaemon(true);
                                                    vIt.start();
                                                }
                                                isSitting = true;
                                                updateLobby = false;
                                                if(pGame == null) {
                                                    pGame = new PokerTableWindow();
                                                    for (Player p : activePlayers) {
                                                        if (p != null && !pp.getName().equals(p.getName())) {
                                                            pGame.getTablePanel().putPlayer(p.getName(), p.getIntBalance(), p.getSeat(), null, false);
                                                            pGame.getTablePanel().getPlayers()[p.getSeat()].hideCards();
                                                            pGame.getTablePanel().makePlayerBet(p.getSeat(), 0);
                                                            pGame.getTablePanel().removeBet(p.getSeat());
                                                            if (!DEBUG) {
                                                                vIt.addPortListener(p.getSeat());
                                                            }
                                                        }
                                                    }
                                                    Platform.runLater(() -> {
                                                        primaryStage.hide();
                                                    });
                                                }
                                                if(Worker.DEBUG) {
                                                    Worker.pGame.getTablePanel().putPlayer(pp.getName(), TakeSeatRequest.parseTableChips(s), thisPlayerSeat = TakeSeatRequest.parseSeat(s), null, true);
                                                } else {
                                                    Worker.pGame.getTablePanel().putPlayer(pp.getName(), TakeSeatRequest.parseTableChips(s), thisPlayerSeat = TakeSeatRequest.parseSeat(s), webcam, true);
                                                    if (webcam != null) {
                                                        vOt = new VideoOutputThread(webcam, zeroVideoPort + thisPlayerSeat);
                                                        vOt.setDaemon(true);
                                                        vOt.setPriority(Thread.MIN_PRIORITY);
                                                        vOt.start();
                                                    }
                                                }
                                                Worker.pGame.getTablePanel().validateIcons(Worker.pGame.getFrame().getSize(), true);
                                                Worker.pGame.getBottomPanel().getActions().setActive(false);
                                                Platform.runLater(()->{
                                                    Worker.pGame.getFrame().setExtendedState(Frame.MAXIMIZED_BOTH);
                                                    //Worker.pGame.getFrame().setBounds(0,0,d0.width,d0.height);
                                                    Worker.pGame.setVisible(true);
                                                });
                                            } else {
                                                int newcomerSeat = TakeSeatRequest.parseSeat(s);
                                                pGame.getTablePanel().putPlayer(TakeSeatRequest.parsePlayerName(s), TakeSeatRequest.parseTableChips(s), newcomerSeat, null, false);
                                                activePorts[TakeSeatRequest.parseSeat(s)] = zeroVideoPort + newcomerSeat;
                                                pGame.getTablePanel().getPlayers()[newcomerSeat].hideCards();
                                                if(!DEBUG) {
                                                    vIt.addPortListener(newcomerSeat);
                                                }
                                                Worker.pGame.getTablePanel().validateIcons(Worker.pGame.getFrame().getSize(), true);
                                                //Worker.pGame.getFrame().setBounds(0,0,d4.width,d4.height);
                                                //Worker.pGame.getFrame().setBounds(0,0,d0.width,d0.height);
                                                Worker.pGame.getFrame().setExtendedState(Frame.MAXIMIZED_BOTH);
                                                if(!DEBUG){
                                                    while(!Worker.pGame.getTablePanel().getPlayers()[newcomerSeat].hasPicture()){}
                                                }
                                            }
                                        }
                                        isFree = true;
                                    } else if (s.contains("startingnewhand")) {
                                        Worker.pGame.getTablePanel().hideAllCards();
                                        Worker.pGame.getTablePanel().clearBets();
                                        Worker.pGame.getTablePanel().hideAndClearPot();
                                        isFree = true;
                                    } else if (s.contains("isdealer")) {
                                        Worker.pGame.getTablePanel().setDealer(Integer.valueOf(s.substring(s.indexOf("=") + 1, s.lastIndexOf("="))));
                                        isFree = true;
                                    } else if (s.contains("action")) {
                                        if (Request.checkSuccess(s)) {
                                            String action = s.substring(s.indexOf("=") + 1, s.lastIndexOf("="));
                                            if (action.equals("postsb")) {
                                                Worker.pGame.getBottomPanel().getActions().setModeBlind(pp.getName(), false);
                                            } else if (action.equals("postbb")) {
                                                Worker.pGame.getBottomPanel().getActions().setModeBlind(pp.getName(), true);
                                                //CFR or XR
                                            } else {
                                                String[] split = s.split("=");
                                                /*if (split[1].equals("CFR")) {
                                                    if((Integer.valueOf(split[2])*tip.getBB()) > Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance()){
                                                        Worker.pGame.bottomPanel.actions.setBetRange(Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance(), Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance());
                                                    } else {
                                                        Worker.pGame.bottomPanel.actions.setBetRange(tip.getBB() * Integer.valueOf(split[2]), Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance());
                                                    }
                                                    Worker.pGame.bottomPanel.actions.setModeCR(pp.getName());
                                                } else */if (split[1].equals("XR")) {
                                                    Worker.pGame.getBottomPanel().getActions().setBetRange(tip.getSB(), Worker.pGame.getTablePanel().getPlayers()[thisPlayerSeat].getBalance());
                                                    Worker.pGame.getBottomPanel().getActions().setModeXR(pp.getName());
                                                } else if (split[1].equals("CR") || split[1].equals("C")){
                                                    /*if((tip.getBB() * 10) > Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance()){
                                                        Worker.pGame.bottomPanel.actions.setBetRange(Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance(), Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance());
                                                    } else {
                                                        Worker.pGame.bottomPanel.actions.setBetRange(tip.getBB() * 10, Worker.pGame.tablePanel.players[thisPlayerSeat].getBalance());
                                                    }
                                                    Worker.pGame.bottomPanel.actions.setModeCR(pp.getName());*/
                                                    if(Integer.valueOf(split[2])>=Worker.pGame.getTablePanel().getPlayers()[thisPlayerSeat].getBalance()){
                                                        Worker.pGame.getBottomPanel().getActions().setModeAllIn(pp.getName());
                                                    }else {
                                                        Worker.pGame.getBottomPanel().getActions().setBetRange(/*tip.getBB() * */ Integer.valueOf(split[2])*2, Worker.pGame.getTablePanel().getPlayers()[thisPlayerSeat].getBalance());
                                                        Worker.pGame.getBottomPanel().getActions().setModeCR(pp.getName());
                                                    }
                                                }
                                            }
                                            Worker.pGame.getBottomPanel().getActions().setActive(true);
                                        }
                                        isFree = true;
                                    } else if (s.contains("postssb")) {
                                        String pName = s.substring(s.indexOf(":") + 1, s.lastIndexOf(":"));
                                        for (int q = 0; q < Worker.pGame.getTablePanel().getPlayers().length; ++q) {
                                            PlayerPanel pp = Worker.pGame.getTablePanel().getPlayers()[q];
                                            if (pp != null && pp.getStats().getNickname().getText().equals(pName)) {
                                                Worker.pGame.getTablePanel().makePlayerBet(q, tip.getSB());
                                                String newBalance = String.valueOf(Integer.valueOf(pp.getStats().getBalance().getText().substring(1)) - tip.getSB());
                                                pp.getStats().getBalance().setText("$" + newBalance);
                                            }
                                        }
                                        isFree = true;
                                    } else if (s.contains("postsbb")) {
                                        String pName = s.substring(s.indexOf(":") + 1, s.lastIndexOf(":"));
                                        for (int q = 0; q < Worker.pGame.getTablePanel().getPlayers().length; ++q) {
                                            PlayerPanel pp = Worker.pGame.getTablePanel().getPlayers()[q];
                                            if (pp != null && pp.getStats().getNickname().getText().equals(pName)) {
                                                Worker.pGame.getTablePanel().makePlayerBet(q, tip.getBB());
                                                String newBalance = String.valueOf(Integer.valueOf(pp.getStats().getBalance().getText().substring(1)) - tip.getBB());
                                                pp.getStats().getBalance().setText("$" + newBalance);
                                            }
                                        }
                                        isFree = true;
                                    } else if (s.contains("isdealt")) {
                                        String[] split = s.split("=");
                                        if (split.length == 3) {
                                            CardLabel cl1 = GetPocketCardsRequest.parseCardLabel(split[1]);
                                            CardLabel cl2 = GetPocketCardsRequest.parseCardLabel(split[2]);
                                            Worker.pGame.getTablePanel().getPlayers()[thisPlayerSeat].showActiveCards(cl1, cl2);
                                            isFree = true;
                                        } else if (split.length == 2) {
                                            if (Integer.valueOf(split[1]) != thisPlayerSeat) {
                                                //NULL POINTER?
                                                Worker.pGame.getTablePanel().getPlayers()[Integer.valueOf(split[1])].showCardback();
                                                isFree = true;
                                            } else {
                                                RequestHandler.send(new GetPocketCardsRequest(pp.getName()));
                                            }
                                        }
                                    } else if (s.contains("fold")) {
                                        String pName = s.substring(s.indexOf(":") + 1, s.lastIndexOf(":"));
                                        for (int q = 0; q < Worker.pGame.getTablePanel().getPlayers().length; ++q) {
                                            PlayerPanel pp = Worker.pGame.getTablePanel().getPlayers()[q];
                                            if (pp != null && pp.getStats().getNickname().getText().equals(pName)) {
                                                Worker.pGame.getTablePanel().getPlayers()[q].hideCards();
                                                break;
                                            }
                                        }
                                        Worker.pGame.getBottomPanel().getChat().getMessageBox().append(String.format("%s folds.\n", pName));
                                        isFree = true;
                                    } else if (s.contains("call")){
                                        String pName = s.substring(s.indexOf(":") + 1, s.lastIndexOf(":"));
                                        int callSum = CallRequest.parseCallSum(s);
                                        /*int max = 0, diff = 0;
                                        for(BetPanel bp : Worker.pGame.tablePanel.betPanels){
                                            int n;
                                            if(bp != null && ((n = Integer.valueOf(bp.betValue.getText().substring(1))) > max)){
                                                max = n;
                                            }
                                        }
                                        for (int q = 0; q < Worker.pGame.tablePanel.players.length; ++q) {
                                            PlayerPanel pp = Worker.pGame.tablePanel.players[q];
                                            if (pp != null && pp.getStats().nickname.getText().equals(pName)) {
                                                diff = max - Worker.pGame.tablePanel.betPanels[q].getBet();
                                                Worker.pGame.tablePanel.changeBet(q, max);
                                                pp.decreaseBalance(diff);
                                                break;
                                            }
                                        }*/
                                        int q = 0;
                                        for(PlayerPanel pp : Worker.pGame.getTablePanel().getPlayers()){
                                            if((pp != null) && pp.getStats().getNickname().getText().equals(pName)){
                                                if(Worker.pGame.getTablePanel().getBetPanels()[q] != null) {
                                                    Worker.pGame.getTablePanel().increaseBet(q, callSum);
                                                } else {
                                                    Worker.pGame.getTablePanel().makePlayerBet(q, callSum);
                                                }
                                                pp.decreaseBalance(callSum);
                                                break;
                                            }
                                            ++q;
                                        }
                                        isFree = true;
                                    } else if(s.contains("raise")){
                                        String pName = s.substring(s.indexOf(":") + 1, s.lastIndexOf(":"));
                                        int raiseSum = Integer.valueOf(s.substring(s.indexOf("=")+1, s.lastIndexOf("=")));
                                        for (int q = 0; q < Worker.pGame.getTablePanel().getPlayers().length; ++q) {
                                            PlayerPanel pp = Worker.pGame.getTablePanel().getPlayers()[q];
                                            if (pp != null && pp.getStats().getNickname().getText().equals(pName)) {
                                                pp.decreaseBalance(raiseSum);
                                                if(Worker.pGame.getTablePanel().getBetPanels()[q]!=null) {
                                                    Worker.pGame.getTablePanel().increaseBet(q, raiseSum);
                                                }else {
                                                    Worker.pGame.getTablePanel().makePlayerBet(q, raiseSum);
                                                }
                                                break;
                                            }
                                        }
                                        isFree = true;
                                    }
                                    else if(s.contains("pot")){
                                        potUpdate = Integer.valueOf(s.substring(s.indexOf("=")+1, s.lastIndexOf("=")));
                                        if(pGame == null) {
                                            pGame = new PokerTableWindow();
                                            for (Player p : activePlayers) {
                                                if (p != null && !pp.getName().equals(p.getName())) {
                                                    pGame.getTablePanel().putPlayer(p.getName(), p.getIntBalance(), p.getSeat(), null, false);
                                                    pGame.getTablePanel().getPlayers()[p.getSeat()].hideCards();
                                                    pGame.getTablePanel().makePlayerBet(p.getSeat(), 0);
                                                    pGame.getTablePanel().removeBet(p.getSeat());
                                                    if (!DEBUG) {
                                                        vIt.addPortListener(p.getSeat());
                                                    }
                                                }
                                            }
                                            pGame.setVisible(true);
                                        }
                                        if(potUpdate > 0) {
                                            pGame.getTablePanel().increasePot(potUpdate);
                                            pGame.getTablePanel().showPot();
                                        }
                                    }
                                    else if(s.contains("waitingforplayers")){
                                        Worker.pGame.getTablePanel().hideAllCards();
                                        Worker.pGame.getTablePanel().clearBets();
                                        Worker.pGame.getTablePanel().hideAndClearPot();
                                        isFree = true;
                                    }
                                    //FLOP
                                    else if(s.contains("dealer")){
                                        int potMoney = 0;
                                        int q = 0;
                                        for(BetPanel bp : Worker.pGame.getTablePanel().getBetPanels()){
                                            if(bp != null){
                                                potMoney += bp.getBet();
                                                Worker.pGame.getTablePanel().removeBet(q);
                                            }
                                            ++q;
                                        }
                                        Worker.pGame.getTablePanel().increasePot(potMoney);
                                        Worker.pGame.getTablePanel().showPot();
                                        String[] cards = Worker.splitStringEvery(s.substring(s.indexOf("=")+1, s.lastIndexOf("=")), 2);
                                        try {
                                            Thread.sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        for(String c : cards){
                                            Worker.pGame.getTablePanel().putBoardCards(GetPocketCardsRequest.parseCardLabel(c));
                                        }
                                        Worker.pGame.getTablePanel().validateIcons(Worker.pGame.getFrame().getSize(), true);
                                        Worker.pGame.getTablePanel().getBoardCards().setNeedUpdate(true);
                                        Worker.pGame.getTablePanel().getBoardCards().updateIcons();
                                        Worker.pGame.getTablePanel().getBoardCards().revalidate();
                                    } else if(s.contains("chat")){
                                        Worker.pGame.getBottomPanel().getChat().getMessageBox().append(ChatMessageRequest.parseChatMessage(s));
                                        isFree = true;
                                    } else if(s.contains("leavesseat")){
                                        String pName = LeaveTableRequest.parseLeftPlayerName(s);
                                        if(pName.equals(pp.getName())){
                                            if(pGame != null) {
                                                pGame.getFrame().dispose();
                                                pGame = null;
                                            }
                                            isSitting = false;
                                            updateLobby = true;
                                            if(Worker.vIt != null){
                                                Worker.vIt.interrupt();
                                                Worker.vIt = null;
                                            }
                                            if(Worker.vOt != null){
                                                Worker.vOt.interrupt();
                                                Worker.vOt = null;
                                            }
                                            RequestHandler.send(new GetPlayerInfoRequest(Worker.pp.getName()));
                                            Platform.runLater(()->{
                                                Worker.primaryStage.show();
                                            });
                                        }
                                        else {
                                            for (int q = 0; q < Worker.pGame.getTablePanel().getPlayers().length; ++q) {
                                                PlayerPanel pp = Worker.pGame.getTablePanel().getPlayers()[q];
                                                if (pp != null && (pp.getStats().getNickname().getText().equals(pName))) {
                                                    if (vIt != null) {
                                                        vIt.removePortListener(q);
                                                    }
                                                    if(Worker.pGame.getTablePanel().getBetPanels()[q] != null) {
                                                        Worker.pGame.getTablePanel().removeBet(q);
                                                        Worker.pGame.getTablePanel().getBetPanels()[q] = null;
                                                    }
                                                    pp.setVisible(false);
                                                    Worker.pGame.getTablePanel().removePlayer(q);
                                                    break;
                                                }
                                            }
                                        }
                                        isFree = true;
                                    } else if (s.contains("check")){
                                        String pName = CheckRequest.parsePlayerName(s);
                                        Worker.pGame.getBottomPanel().getChat().getMessageBox().append(String.format("%s checks.\n", pName));
                                        isFree = true;
                                    } else if (s.contains("wins")){
                                        String pName = s.substring(s.indexOf(":")+1, s.lastIndexOf(":"));
                                        int winAmount = Integer.valueOf(s.substring(s.indexOf("=")+1, s.lastIndexOf("=")));
                                        Worker.pGame.getBottomPanel().getChat().getMessageBox().append(String.format("%s WINS $%d!\n", pName, winAmount));
                                        int q = 0;
                                        Worker.pGame.getTablePanel().clearBets();
                                        for(PlayerPanel pp : Worker.pGame.getTablePanel().getPlayers()){
                                            if(pp != null && pp.getStats().getNickname().getText().equals(pName)) {
                                                Worker.pGame.getTablePanel().hideAndClearPot();
                                                Worker.pGame.getTablePanel().changeBet(q, winAmount);
                                                try {
                                                    Thread.sleep(1500);
                                                } catch (InterruptedException e) {
                                                    e.printStackTrace();
                                                }
                                                pp.increaseBalance(winAmount);
                                            }
                                            ++q;
                                        }
                                        isFree = true;
                                    } else if(s.contains("getplayerinfo")){
                                        int newBalance = Integer.parseInt(s.substring(s.indexOf("=")+1, s.lastIndexOf("=")));
                                        pp.setBalance(newBalance);
                                    }
                                }
                                System.out.println("}");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
            });
            t.setDaemon(true);
            t.setPriority(Thread.MAX_PRIORITY);
            t.start();
        }
    }
    private static String[] splitStringEvery(String s, int interval) {
        int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
        String[] result = new String[arrayLength];

        int j = 0;
        int lastIndex = result.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            result[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        result[lastIndex] = s.substring(j);

        return result;
    }
}

