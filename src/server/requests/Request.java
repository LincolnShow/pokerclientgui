package server.requests;

public abstract class Request {
    protected char receiver;

    protected Request(char receiver) {
        this.receiver = receiver;
    }


    public static boolean checkSuccess(String answer) {
        return !answer.contains("FAIL");
    }
}
