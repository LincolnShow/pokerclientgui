package server.requests;

import server.RequestHandler;

public class LoginRequest extends Request {
    private String login, password;

    public LoginRequest(String login, String password) {
        super('A');
        this.login = login;
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:login=%s=;", receiver, login, password);
    }

    public static int parseBalance(Answer a) {
        try {
            return Integer.parseInt(a.data.split("=")[1]);
        } catch (Exception ignored) {
        }
        return -1;
    }
}
