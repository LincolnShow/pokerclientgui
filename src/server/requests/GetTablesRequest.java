package server.requests;

import lobby.Table;

import java.util.LinkedList;
import java.util.List;

public class GetTablesRequest extends Request {
    private String login;

    public GetTablesRequest(String login) {
        super('A');
        this.login = login;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:gettables==;", receiver, login);
    }

    public static List<Table> parseTables(Answer a) {
        try {
            List<Table> result = new LinkedList<>();
            String[] split = a.data.split(";");
            for (int i = 1; i < split.length; ++i) {
                String tableName = split[i].substring(split[i].indexOf(':') + 1, split[i].indexOf("table") - 1);
                int highStake = Integer.parseInt(split[i].substring(split[i].indexOf("NL") + 2, split[i].indexOf('_')))/100;
                String stakes = String.format("$%d/$%d", highStake / 2, highStake);
                String playersInfo = String.format("%s/6", split[i].split("=")[1]);
                result.add(new Table(tableName, stakes, playersInfo));
            }
            return result;
        } catch (Exception ignored) {
        }
        return null;
    }

    public static Table parseTable(String data) {
        String tableName = data.substring(data.indexOf(':') + 1, data.indexOf("table") - 1);
        int highStake = Integer.parseInt(data.substring(data.indexOf("NL") + 2, data.indexOf('_')))/100;
        String stakes = String.format("$%d/$%d", highStake / 2, highStake);
        String playersInfo = String.format("%s/6", data.split("=")[1]);
        return new Table(tableName, stakes, playersInfo);
    }

}
