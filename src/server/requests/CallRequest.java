package server.requests;

public class CallRequest extends Request {
    private String name;

    public CallRequest(String name) {
        super('D');
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:call==;", receiver, name);
    }

    public static int parseCallSum(String s){
        return Integer.valueOf(s.substring(s.indexOf("=")+1, s.lastIndexOf("=")));
    }
}
