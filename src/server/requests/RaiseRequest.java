package server.requests;

public class RaiseRequest extends Request {
    private String name;
    private int sum;

    public RaiseRequest(String name, int sum) {
        super('D');
        this.name = name;
        this.sum = sum;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:raise=%d=;", receiver, name, sum);
    }
}
