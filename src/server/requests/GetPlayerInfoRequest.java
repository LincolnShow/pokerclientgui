package server.requests;

public class GetPlayerInfoRequest extends Request {
    private String playerName;
    public GetPlayerInfoRequest(String playerName) {
        super('A');
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:getplayerinfo==;", receiver, playerName);
    }
}
