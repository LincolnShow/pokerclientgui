package server.requests;

public class ChatMessageRequest extends Request {
    private String message;
    private String name;
    public ChatMessageRequest(String name, String message) {
        super('C');
        this.message = message;
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:chat=%s=;", receiver, name, message);
    }
    public static String parseChatMessage(String s){
        s = s.trim();
        return String.format("%s: %s\n",
                s.substring(s.indexOf(":")+1, s.lastIndexOf(":")),
                s.substring(s.indexOf("=")+1, s.lastIndexOf("=")));
    }
}
