package server.requests;


public class CheckRequest extends Request {
    private String pName;
    public CheckRequest(String pName) {
        super('D');
        this.pName = pName;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:check==;", receiver, pName);
    }
    public static String parsePlayerName(String s){
        return s.substring(s.indexOf(":")+1, s.lastIndexOf(":"));
    }
}
