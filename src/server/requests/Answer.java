package server.requests;

public class Answer {
    private final boolean isSuccess;
    public String data;

    public Answer(boolean isSuccess, String data) {
        this.isSuccess = isSuccess;
        this.data = data;
    }
}
