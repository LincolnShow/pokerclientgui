package server.requests;

public class LeaveTableRequest extends Request {
    private String playerName;
    public LeaveTableRequest(String playerName) {
        super('A');
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:leavesseat==;", receiver, playerName);
    }
    public static String parseLeftPlayerName(String s){
        return s.substring(s.indexOf(":")+1, s.lastIndexOf(":"));
    }
}
