package server.requests;

public class PostsBbRequest extends Request {
    private String playerName;

    public PostsBbRequest(String playerName) {
        super('D');
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:postsbb==;", receiver, playerName);
    }
}
