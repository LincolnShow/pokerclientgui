package server.requests;

import table.CardInformation;
import table.CardLabel;

public class GetPocketCardsRequest extends Request {
    private String name;

    public GetPocketCardsRequest(String name) {
        super('A');
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:getpocketcards==;", receiver, name);
    }

    public static CardLabel parseCardLabel(String s) {
        char cv = s.charAt(0);
        int cardValue;
        if (cv == 'T' || cv == 't') {
            cardValue = 10;
        } else if (cv == 'J' || cv == 'j') {
            cardValue = 11;
        } else if (cv == 'Q' || cv == 'q') {
            cardValue = 12;
        } else if (cv == 'K' || cv == 'k') {
            cardValue = 13;
        } else if (cv == 'A' || cv == 'a') {
            cardValue = 14;
        } else {
            cardValue = Character.getNumericValue(cv);
        }
        char cs = s.charAt(1);
        CardInformation.CardSuit cardSuit = null;
        switch (cs) {
            case 's':
                cardSuit = CardInformation.CardSuit.SPADES;
                break;
            case 'h':
                cardSuit = CardInformation.CardSuit.HEARTS;
                break;
            case 'd':
                cardSuit = CardInformation.CardSuit.DIAMONDS;
                break;
            case 'c':
                cardSuit = CardInformation.CardSuit.CLUBS;
                break;
        }
        return new CardLabel(cardValue, cardSuit);
    }
}
