package server.requests;

import lobby.Player;

public class TakeSeatRequest extends Request {
    private String tableName;
    private String placeNumber;
    private String chipsAmount;

    public TakeSeatRequest(String tableName, String placeNumber, String chipsAmount) {
        super('A');
        this.tableName = tableName;
        this.placeNumber = placeNumber;
        this.chipsAmount = chipsAmount;
    }


    @Override
    public String toString() {
        return String.format("%c:%s:takesseat=%s=%s;", receiver, tableName, placeNumber, chipsAmount);
    }

    public static int parseSeat(String s) {
        return Integer.valueOf(s.substring(s.indexOf("=")+1, s.lastIndexOf("=")));
    }

    public static int parseTableChips(String s) {
        return Integer.valueOf(s.substring(s.lastIndexOf("=") + 1));
    }

    public static String parsePlayerName(String s) {
        return s.substring(s.indexOf(':') + 1, s.lastIndexOf(':'));
    }
}
