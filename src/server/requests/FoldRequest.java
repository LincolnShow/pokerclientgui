package server.requests;

public class FoldRequest extends Request {
    private String name;

    public FoldRequest(String name) {
        super('D');
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:fold==;", receiver, name);
    }
}
