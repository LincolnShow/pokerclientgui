package server.requests;

import lobby.Player;

import java.util.LinkedList;
import java.util.List;

public class GetTableInfoRequest extends Request {
    private String tableName;

    public GetTableInfoRequest(String tableName) {
        super('A');
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:gettableinfo==;", receiver, tableName);
    }

    public static List<Player> parseActivePlayers(Answer a) {
        try {
            List<Player> pList = new LinkedList<>();
            String[] split = a.data.split(";");
            for (int i = 1; i < split.length; ++i) {
                String[] subSplit = split[i].split(":");
                String pName = subSplit[1];
                String active = subSplit[2].substring(0, subSplit[2].indexOf('='));
                if (active.equals("active")) {
                    int pSeat = Integer.parseInt(subSplit[2].substring(subSplit[2].indexOf('=') + 1, subSplit[2].lastIndexOf('=')));
                    String pTableChips = "$" + subSplit[2].substring(subSplit[2].lastIndexOf('=') + 1);
                    pList.add(new Player(pName, pTableChips, "http://images.clipartbro.com/67/shaded-yellow-star-clip-art-vector-online-royalty-free-67032.png", pSeat));
                }
            }
            return pList;
        } catch (Exception ignored) {
        }
        return null;
    }

    public static Player parsePlayer(String data) {
        String[] subSplit = data.split(":");
        String pName = subSplit[1];
        int pSeat = Integer.parseInt(subSplit[2].substring(subSplit[2].indexOf('=') + 1, subSplit[2].lastIndexOf('=')));
        String pTableChips = "$" + subSplit[2].substring(subSplit[2].lastIndexOf('=') + 1);
        return new Player(pName, pTableChips, "http://images.clipartbro.com/67/shaded-yellow-star-clip-art-vector-online-royalty-free-67032.png", pSeat);
    }
}
