package server.requests;

public class PostsSbRequest extends Request {
    private String playerName;

    public PostsSbRequest(String playerName) {
        super('D');
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return String.format("%c:%s:postssb==;", receiver, playerName);
    }
}
