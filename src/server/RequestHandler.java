package server;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import lobby.GameClient;
import server.requests.Request;

import javax.swing.*;
import java.io.IOException;

public abstract class RequestHandler {
    public synchronized static void send(Request request) {
        try {
            Worker.server.getOutputStream().write(request.toString().trim().getBytes());
        } catch (IOException e) {
            Platform.runLater(()->{
                Alert a = new Alert(Alert.AlertType.WARNING,"Connection Lost");
                a.getDialogPane().getStylesheets().add(GameClient.class.getResource("loginDialog.css").toExternalForm());
                a.showAndWait();
                System.out.println(e.getLocalizedMessage());
                Platform.exit();
                System.exit(1);
            });
            while(true){}
        }
    }
    public synchronized static void sendRaw(byte ... data){
        try {
            Worker.server.getOutputStream().write(data);
        } catch (IOException e) {
            Platform.runLater(()->{
                Alert a = new Alert(Alert.AlertType.WARNING,"Connection Lost");
                a.setHeaderText("ERROR");
                a.getDialogPane().getStylesheets().add(GameClient.class.getResource("loginDialog.css").toExternalForm());
                a.showAndWait();
                System.out.println(e.getLocalizedMessage());
                Platform.exit();
                System.exit(1);
            });
            while(true){}
        }
    }
}
