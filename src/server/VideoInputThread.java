package server;

import table.Constants;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.*;

public class VideoInputThread extends Thread implements Constants {
    private volatile boolean isRunning = false;
    private volatile DatagramSocket[] portsToListen = new DatagramSocket[6];

    public synchronized void addPortListener(int position) {
        if (portsToListen[position] != null) {
            portsToListen[position].close();
        }
        try {
            portsToListen[position] = new DatagramSocket(zeroVideoPort + position);
        } catch (SocketException e) {
            //e.printStackTrace();
        }
    }

    public synchronized void removePortListener(int position) {
        if (portsToListen[position] != null) {
            portsToListen[position].close();
            portsToListen[position] = null;
        }
    }

    @Override
    public void run() {
        setName("VideoInputThread");
        isRunning = true;
        byte[] fromServerData = new byte[16384];
        DatagramPacket fromServer = new DatagramPacket(fromServerData, fromServerData.length);
        BufferedImage img;
        InputStream byteInputStream;
        while (isRunning) {
            try {
                for (int i = 0; i < portsToListen.length; ++i) {
                    if (portsToListen[i] != null) {
                        portsToListen[i].receive(fromServer);
                        byteInputStream = new ByteArrayInputStream(fromServer.getData());
                        img = ImageIO.read(byteInputStream);
                        Worker.pGame.getTablePanel().getPlayers()[i].setVideoLabelImage(img);
                    }
                }
                Thread.sleep(16);
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    @Override
    public void interrupt() {
        isRunning = false;
    }

    @Override
    public boolean isInterrupted() {
        return !isRunning;
    }
}
