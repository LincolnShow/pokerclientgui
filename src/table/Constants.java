package table;

import java.awt.*;

public interface Constants {
    //String backgroundImagePath = "out/production/PokerClientGUI/com/assets/game-background.jpg";
    //String tableImagePath = "out/production/PokerClientGUI/com/assets/table.png";
    //String cardbackImagePath = "out/production/PokerClientGUI/com/assets/cardback.png";
    //String assetsPath = "out/production/PokerClientGUI/com/assets/";
    //Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    /*final static Dimension d0 = new Dimension(){{setSize(screen.width/100*40, screen.height/100*70);}};
    final static Dimension d1 = new Dimension(){{setSize(screen.width/100*45, screen.height/100*75);}};
    final static Dimension d2 = new Dimension(){{setSize(screen.width/100*55, screen.height/100*80);}};
    final static Dimension d3 = new Dimension(){{setSize(screen.width/100*65, screen.height/100*85);}};
    final static Dimension d4 = new Dimension(){{setSize(screen.width/100*75, screen.height/100*95);}};
    final static Dimension d5 = new Dimension(){{setSize(screen.width/100*80, screen.height/100*85);}};
    final static Dimension d6 = new Dimension(){{setSize(screen.width/100*85, screen.height/100*90);}};*/
    int zeroVideoPort = 30000;
    Dimension d0 = new Dimension() {{
        setSize(640, 480);
    }};
    Dimension d1 = new Dimension() {{
        setSize(d0.width * 1.05, d0.height * 1.10);
    }};
    Dimension d2 = new Dimension() {{
        setSize(d1.width * 1.05, d1.height * 1.10);
    }};
    Dimension d3 = new Dimension() {{
        setSize(d2.width * 1.05, d2.height * 1.10);
    }};
    Dimension d4 = new Dimension() {{
        setSize(d3.width * 1.05, d3.height * 1.10);
    }};
    Dimension d5 = new Dimension() {{
        setSize(d4.width * 1.05, d4.height * 1.10);
    }};
    Dimension d6 = new Dimension() {{
        setSize(d5.width * 1.05, d5.height * 1.10);
    }};
}
