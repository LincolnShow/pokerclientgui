package table;

import server.RequestHandler;
import server.Worker;
import server.requests.ChatMessageRequest;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ChatPanel extends JPanel {
    private JTextArea messageBox;
    private JTextField newMessageField;
    public ChatPanel() {
        super();
        setBackground(Color.GRAY);
        setOpaque(true);
        SpringLayout layout = new SpringLayout();
        setLayout(layout);

        setMessageBox(new JTextArea(""));
        getMessageBox().setWrapStyleWord(true);
        getMessageBox().setLineWrap(true);
        getMessageBox().setEditable(false);
        JScrollPane messageScrollPane = new JScrollPane(getMessageBox(), ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        getMessageBox().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                getMessageBox().setCaretPosition(getMessageBox().getDocument().getLength());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        setNewMessageField(new JTextField());
        JButton sendButton = new JButton("Send");
        add(messageScrollPane);
        add(getNewMessageField());
        add(sendButton);
        layout.putConstraint(SpringLayout.NORTH, messageScrollPane, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, messageScrollPane, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, messageScrollPane, 0, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.SOUTH, messageScrollPane, 0, SpringLayout.NORTH, getNewMessageField());
        layout.putConstraint(SpringLayout.SOUTH, getNewMessageField(), 0, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.EAST, getNewMessageField(), 0, SpringLayout.WEST, sendButton);
        layout.putConstraint(SpringLayout.WEST, getNewMessageField(), 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, sendButton, 0, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, sendButton, 0, SpringLayout.SOUTH, messageScrollPane);
        layout.putConstraint(SpringLayout.SOUTH, sendButton, 0, SpringLayout.SOUTH, this);

        getNewMessageField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    /*RequestHandler.send(new Request('A') {
                        String reqS = newMessageField.getText().trim();

                        @Override
                        public String toString() {
                            return reqS;
                        }
                    });*/
                    RequestHandler.send(new ChatMessageRequest(Worker.pp.getName(), getNewMessageField().getText().trim()));
                    getNewMessageField().setText("");
                }
            }
        });
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*
                RequestHandler.send(new Request('A') {
                    String reqS = newMessageField.getText().trim();

                    @Override
                    public String toString() {
                        return reqS;
                    }
                });*/
                RequestHandler.send(new ChatMessageRequest(Worker.pp.getName(), getNewMessageField().getText().trim()));
                getNewMessageField().setText("");
            }
        });
    }

    public JTextArea getMessageBox() {
        return messageBox;
    }

    public void setMessageBox(JTextArea messageBox) {
        this.messageBox = messageBox;
    }

    public JTextField getNewMessageField() {
        return newMessageField;
    }

    public void setNewMessageField(JTextField newMessageField) {
        this.newMessageField = newMessageField;
    }
}
