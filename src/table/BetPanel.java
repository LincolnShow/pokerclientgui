package table;


import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class BetPanel extends JPanel implements Constants {
    private JLabel betImage = new JLabel();
    private JLabel betValue = new JLabel("$0");
    private BufferedImage img;
    private SpringLayout layout;

    public BetPanel() {
        super();
        layout = new SpringLayout();
        setLayout(layout);
        add(getBetImage());
        add(getBetValue());
        try {
            img = ImageIO.read(getClass().getResource("bet.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        getBetImage().setIcon(new ImageIcon(img));
        setPreferredSize(new Dimension(img.getWidth(), img.getHeight() + 30));
        getBetValue().setHorizontalAlignment(SwingConstants.CENTER);
        getBetValue().setVerticalAlignment(SwingConstants.CENTER);
        getBetValue().setForeground(Color.ORANGE);
        getBetImage().setOpaque(false);
        setOpaque(false);
        layout.putConstraint(SpringLayout.NORTH, getBetImage(), 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, getBetValue(), 1, SpringLayout.SOUTH, getBetImage());
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, getBetImage(), 0, SpringLayout.HORIZONTAL_CENTER, this);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, getBetValue(), 0, SpringLayout.HORIZONTAL_CENTER, this);

        /*setBorder(BorderFactory.createLineBorder(Color.GREEN));
        betImage.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        betValue.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                try {
                    getBetImage().setIcon(new ImageIcon(Thumbnails.of(img).size((int) (getWidth() / 2.2), getHeight()).asBufferedImage()));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                Dimension cur = TablePanel.getCurrentDim();
                if (cur == d0) {
                    getBetValue().setFont(new Font("Tahoma", Font.BOLD, 10));
                } else if (cur == d1) {
                    getBetValue().setFont(new Font("Tahoma", Font.BOLD, 12));
                } else if (cur == d2) {
                    getBetValue().setFont(new Font("Tahoma", Font.BOLD, 14));
                } else if (cur == d3) {
                    getBetValue().setFont(new Font("Tahoma", Font.BOLD, 16));
                } else if (cur == d4) {
                    getBetValue().setFont(new Font("Tahoma", Font.BOLD, 18));
                } else if (cur == d5) {
                    getBetValue().setFont(new Font("Tahoma", Font.BOLD, 20));
                } else if (cur == d6) {
                    getBetValue().setFont(new Font("Tahoma", Font.BOLD, 22));
                }

            }
        });
    }
    public int getBet(){
        return Integer.valueOf(getBetValue().getText().substring(1));
    }

    public JLabel getBetImage() {
        return betImage;
    }

    public void setBetImage(JLabel betImage) {
        this.betImage = betImage;
    }

    public JLabel getBetValue() {
        return betValue;
    }

    public void setBetValue(JLabel betValue) {
        this.betValue = betValue;
    }
}
