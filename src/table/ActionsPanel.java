package table;

import server.RequestHandler;
import server.Worker;
import server.requests.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

public class ActionsPanel extends JPanel {
    private final static String PLAY_PANEL = "This player's turn";
    private final static String WAIT_PANEL = "Other player's turn";
    private CardLayout cl;
    private JSlider betSlider;
    private JTextField betField;
    private int minBet, maxBet;
    private JButton actionb1;
    private JButton actionb2;
    private JButton actionb3;
    private JButton exitButton;
    private int sbVal = 0;

    public ActionsPanel() {
        super();
        cl = new CardLayout();
        setLayout(cl);
        setOpaque(false);
        //PLAY PANEL SETUP
        SpringLayout playLayout = new SpringLayout();
        JPanel playPanel = new JPanel();
        playPanel.setBackground(new Color(150, 0, 0));
        playPanel.setOpaque(true);
        playPanel.setLayout(playLayout);
        setActionb1(new JButton("Fold"));
        getActionb1().setFont(getActionb1().getFont().deriveFont(Font.BOLD, 15));
        setActionb2(new JButton("Call"));
        getActionb2().setFont(getActionb2().getFont().deriveFont(Font.BOLD, 15));
        actionb3 = new JButton("Raise");
        actionb3.setFont(actionb3.getFont().deriveFont(Font.BOLD, 15));
        betSlider = new JSlider(SwingConstants.HORIZONTAL);
        setBetRange(150, 20000);
        betField = new JTextField();
        betField.setText(String.valueOf(minBet));
        betField.setHorizontalAlignment(SwingConstants.CENTER);
        betField.setFont(betField.getFont().deriveFont(16.0f));
        betField.addActionListener(e -> {
            betField.transferFocus();
        });
        betField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    int i = Integer.parseInt(betField.getText());
                    if (i < minBet) {
                        betField.setText(String.valueOf(minBet));
                    } else if (i > maxBet) {
                        betField.setText(String.valueOf(maxBet));
                    }
                } catch (NumberFormatException ignored) {
                }
            }
        });
        betField.getDocument().addDocumentListener(new DocumentListener() {
            private boolean validating = false;

            @Override
            public void insertUpdate(DocumentEvent e) {
                if (!validating) {
                    SwingUtilities.invokeLater(this::validateBet);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (!validating) {
                    SwingUtilities.invokeLater(this::validateBet);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (!validating) {
                    SwingUtilities.invokeLater(this::validateBet);
                }
            }

            public void validateBet() {
                validating = true;
                try {
                    int i = Integer.parseInt(betField.getText());
                    betSlider.setValue(i);
                } catch (NumberFormatException ignored) {
                }
                validating = false;
            }
        });
        betSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!betField.hasFocus()) {
                    betField.setText(String.valueOf(betSlider.getValue()));
                }
            }
        });
        playPanel.add(getActionb1());
        playPanel.add(getActionb2());
        playPanel.add(actionb3);
        playPanel.add(betField);
        playPanel.add(betSlider);
        playLayout.putConstraint(SpringLayout.WEST, getActionb1(), 1, SpringLayout.WEST, playPanel);
        playLayout.putConstraint(SpringLayout.WEST, getActionb2(), 5, SpringLayout.EAST, getActionb1());
        playLayout.putConstraint(SpringLayout.WEST, actionb3, 5, SpringLayout.EAST, getActionb2());
        playLayout.putConstraint(SpringLayout.NORTH, betSlider, 10, SpringLayout.SOUTH, getActionb2());
        playLayout.putConstraint(SpringLayout.WEST, betSlider, 0, SpringLayout.WEST, getActionb1());
        playLayout.putConstraint(SpringLayout.EAST, betSlider, 0, SpringLayout.EAST, actionb3);
        playLayout.putConstraint(SpringLayout.NORTH, betField, 10, SpringLayout.SOUTH, betSlider);
        playLayout.putConstraint(SpringLayout.WEST, betField, -20, SpringLayout.WEST, getActionb2());
        playLayout.putConstraint(SpringLayout.EAST, betField, 20, SpringLayout.EAST, getActionb2());
        playPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                playPanel.invalidate();
                int spaceH = getWidth() - (5 * 2 + 2);
                int spaceV = (getHeight() - 2 - 10 - 10);
                Dimension bSize = new Dimension(spaceH / 3, spaceV / 2);
                getActionb1().setPreferredSize(bSize);
                getActionb1().setMinimumSize(bSize);
                getActionb2().setPreferredSize(bSize);
                getActionb2().setMinimumSize(bSize);
                actionb3.setPreferredSize(bSize);
                actionb3.setMinimumSize(bSize);
                betField.setPreferredSize(new Dimension(-1, spaceV / 4));
                betSlider.setPreferredSize(new Dimension(-1, spaceV / 4));
            }
        });
        //WAIT PANEL SETUP
        JPanel waitPanel = new JPanel();
        setExitButton(new JButton("Leave\n table"));
        getExitButton().setPreferredSize(new Dimension(100, 50));
        getExitButton().setMaximumSize(getExitButton().getPreferredSize());
        waitPanel.add(getExitButton());
        waitPanel.setOpaque(false);
        add(playPanel, PLAY_PANEL);
        add(waitPanel, WAIT_PANEL);
        cl.show(this, PLAY_PANEL);
    }

    public void setActive(boolean isActive) {
        cl.show(this, isActive ? PLAY_PANEL : WAIT_PANEL);
    }

    public void setBetRange(int min, int max) {
        minBet = min;
        maxBet = max;
        betSlider.setMinimum(minBet);
        betSlider.setMaximum(maxBet);
        betSlider.setValue(minBet);
    }

    public void setModeBlind(String playerName, boolean isBB) {
        getActionb1().setVisible(false);
        actionb3.setVisible(false);
        betSlider.setVisible(false);
        betField.setVisible(false);
        for (ActionListener al : getActionb2().getActionListeners()) {
            getActionb2().removeActionListener(al);
        }
        getActionb2().setText(isBB ? "BB" : "SB");
        getActionb2().addActionListener(e -> {
            RequestHandler.send(isBB ? new PostsBbRequest(playerName) : new PostsSbRequest(playerName));
            setActive(false);
        });
    }

    public void setModeCR(String playerName) {
        getActionb1().setText("Fold");
        for (ActionListener al : getActionb1().getActionListeners()) {
            getActionb1().removeActionListener(al);
        }
        getActionb1().addActionListener(e -> {
            RequestHandler.send(new FoldRequest(playerName));
            setActive(false);
        });
        getActionb2().setText("Call");
        for (ActionListener al : getActionb2().getActionListeners()) {
            getActionb2().removeActionListener(al);
        }
        getActionb2().addActionListener(e -> {
            RequestHandler.send(new CallRequest(playerName));
            setActive(false);
        });
        actionb3.setText("Raise");
        for (ActionListener al : actionb3.getActionListeners()) {
            actionb3.removeActionListener(al);
        }
        actionb3.addActionListener(e -> {
            RequestHandler.send(new RaiseRequest(playerName, Integer.valueOf(betField.getText())));
            setActive(false);
        });

        getActionb1().setVisible(true);
        getActionb2().setVisible(true);
        actionb3.setVisible(true);
        betSlider.setVisible(true);
        betField.setVisible(true);
    }

    public void setModeXR(String playerName) {
        getActionb1().setText("Fold");
        for (ActionListener al : getActionb1().getActionListeners()) {
            getActionb1().removeActionListener(al);
        }
        getActionb1().addActionListener(e -> {
            RequestHandler.send(new FoldRequest(playerName));
            setActive(false);
        });
        getActionb2().setText("Check");
        for (ActionListener al : getActionb2().getActionListeners()) {
            getActionb2().removeActionListener(al);
        }
        getActionb2().addActionListener(e -> {
            RequestHandler.send(new CheckRequest(Worker.pp.getName()));
            setActive(false);
        });
        actionb3.setText("Raise");
        for (ActionListener al : actionb3.getActionListeners()) {
            actionb3.removeActionListener(al);
        }
        actionb3.addActionListener(e -> {
            RequestHandler.send(new RaiseRequest(playerName, Integer.valueOf(betField.getText())));
            setActive(false);
        });

        getActionb1().setVisible(true);
        getActionb2().setVisible(true);
        actionb3.setVisible(true);
        betSlider.setVisible(true);
        betField.setVisible(true);
    }
    public void setModeAllIn(String playerName){
        actionb3.setVisible(false);
        betSlider.setVisible(false);
        betField.setVisible(false);
        actionb1.setText("Fold");
        for (ActionListener al : getActionb1().getActionListeners()) {
            actionb1.removeActionListener(al);
        }
        getActionb1().addActionListener(e -> {
            RequestHandler.send(new FoldRequest(playerName));
            setActive(false);
        });
        actionb1.setVisible(true);
        for (ActionListener al : getActionb2().getActionListeners()) {
            getActionb2().removeActionListener(al);
        }
        getActionb2().setText("ALL\n IN");
        getActionb2().addActionListener(e -> {
            RequestHandler.send(new CallRequest(playerName));
            setActive(false);
        });
    }

    public JButton getActionb1() {
        return actionb1;
    }

    public void setActionb1(JButton actionb1) {
        this.actionb1 = actionb1;
    }

    public JButton getActionb2() {
        return actionb2;
    }

    public void setActionb2(JButton actionb2) {
        this.actionb2 = actionb2;
    }

    public JButton getExitButton() {
        return exitButton;
    }

    public void setExitButton(JButton exitButton) {
        this.exitButton = exitButton;
    }

    public int getSbVal() {
        return sbVal;
    }

    public void setSbVal(int sbVal) {
        this.sbVal = sbVal;
    }
}
