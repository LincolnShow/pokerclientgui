package table;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class BottomPanel extends JPanel {
    private ChatPanel chat;
    private ActionsPanel actions;

    public BottomPanel() {
        super();
        SpringLayout layout = new SpringLayout();
        setLayout(layout);
        setBackground(new Color(80, 0, 0));
        setOpaque(true);
        setChat(new ChatPanel());
        getChat().setVisible(true);
        add(getChat());
        setActions(new ActionsPanel());
        getActions().setVisible(true);
        add(getActions());
        layout.putConstraint(SpringLayout.SOUTH, this, 1, SpringLayout.SOUTH, getChat());
        layout.putConstraint(SpringLayout.EAST, getActions(), 0, SpringLayout.EAST, this);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED),
                BorderFactory.createBevelBorder(BevelBorder.LOWERED)));

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                revalidate();
                Rectangle r = getBounds();
                getChat().setPreferredSize(new Dimension(r.width / 3, 125));
                getActions().setPreferredSize(new Dimension(r.width / 3, 125));
            }
        });
    }

    public ChatPanel getChat() {
        return chat;
    }

    public void setChat(ChatPanel chat) {
        this.chat = chat;
    }

    public ActionsPanel getActions() {
        return actions;
    }

    public void setActions(ActionsPanel actions) {
        this.actions = actions;
    }
}
