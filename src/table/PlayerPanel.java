package table;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import net.coobird.thumbnailator.Thumbnails;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class PlayerPanel extends JLayeredPane implements Constants {
    private JPanel video;
    private JLabel videoLabel;
    private StatsPanel stats;
    private JLabel cardback;
    private BoardCardsPanel playerCards;
    private JLabel dealerLabel;
    private BetPanel betPanel;

    public PlayerPanel(String name, int balance, int orientation, Webcam activeWebcam, boolean active) {
        super();
        SpringLayout layout = new SpringLayout();
        setLayout(layout);
        stats = new StatsPanel(name, String.valueOf(balance));
        dealerLabel = new JLabel();
        dealerLabel.setVisible(false);
        add(dealerLabel, new Integer(10));
        //dealerLabel.setIcon(new ImageIcon(assetsPath + "5/chips/chip-d.png"));

        if (activeWebcam != null || active) {
            if (activeWebcam != null) {
                video = new WebcamPanel(activeWebcam);
                ((WebcamPanel) video).setFPSDisplayed(false);
                ((WebcamPanel) video).start();
            } else {
                video = new JPanel();
            }
            video.setBorder(BorderFactory.createLineBorder(Color.GREEN));
            video.setLayout(new FlowLayout());
            playerCards = new BoardCardsPanel();
            //playerCards.setBorder(BorderFactory.createLineBorder(Color.GREEN));
            //REMOVE BEGIN
            //playerCards.putCards(new CardLabel(14, CardInformation.CardSuit.DIAMONDS));
            //playerCards.putCards(new CardLabel(14, CardInformation.CardSuit.CLUBS));
            //REMOVE END
            playerCards.setIconPath("6/cards.png");
            add(playerCards, new Integer(1));
            //TO DO
            //Pass playerCards to another communication thread
            stats.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    playerCards.updateIconsPref(stats.getWidth() + 20, stats.getHeight() + 10);
                    layout.putConstraint(SpringLayout.EAST, dealerLabel, -stats.getWidth() / 12, SpringLayout.EAST, stats);
                    layout.putConstraint(SpringLayout.NORTH, dealerLabel, stats.getHeight() / 2 - dealerLabel.getHeight() / 2, SpringLayout.NORTH, stats);
                    revalidate();
                }
            });

            Timer t1 = new Timer(1, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    layout.putConstraint(SpringLayout.WEST, playerCards, stats.getWidth() / 2 - playerCards.getWidth() / 2, SpringLayout.WEST, stats);
                    layout.putConstraint(SpringLayout.NORTH, playerCards, (int) -(playerCards.getHeight() / 1.8), SpringLayout.NORTH, stats);
                    revalidate();
                }
            });
            t1.setRepeats(false);
            playerCards.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    t1.restart();
                }
            });
        } else {
            video = new JPanel();
            SpringLayout sl = new SpringLayout();
            video.setLayout(sl);
            videoLabel = new JLabel();
            video.add(videoLabel);
            video.setBorder(BorderFactory.createLineBorder(Color.GRAY));
            sl.putConstraint(SpringLayout.NORTH, videoLabel, 0, SpringLayout.NORTH, video);
            sl.putConstraint(SpringLayout.WEST, videoLabel, 0, SpringLayout.WEST, video);
            sl.putConstraint(SpringLayout.SOUTH, videoLabel, 0, SpringLayout.SOUTH, video);
            sl.putConstraint(SpringLayout.EAST, videoLabel, 0, SpringLayout.EAST, video);

            cardback = new JLabel();
            cardback.setOpaque(false);
            add(cardback, new Integer(1));
            stats.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    layout.putConstraint(SpringLayout.WEST, cardback, stats.getWidth() / 2 - cardback.getWidth() / 2, SpringLayout.WEST, stats);
                    layout.putConstraint(SpringLayout.NORTH, cardback, -(cardback.getHeight() / 3), SpringLayout.NORTH, stats);

                    layout.putConstraint(SpringLayout.EAST, dealerLabel, -stats.getWidth() / 12, SpringLayout.EAST, stats);
                    layout.putConstraint(SpringLayout.NORTH, dealerLabel, stats.getHeight() / 2 - dealerLabel.getHeight() / 2, SpringLayout.NORTH, stats);
                    revalidate();
                }
            });
        }
        Timer t = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // (original height / original width) x new width = new height
                // new neight / (original height / original width) = new width
                // (1200 / 1600) x 400 = 300
                int newW = PlayerPanel.this.getWidth() - stats.getWidth() - 2;
                int newH = (int) ((144.0 / 176.0) * newW);
                if (newH > PlayerPanel.this.getHeight()) {
                    newH = PlayerPanel.this.getHeight() - 2;
                    newW = (int) (newH / (144.0 / 176.0));
                }
                video.setPreferredSize(new Dimension(newW, newH));
                video.revalidate();
            }
        });
        t.setRepeats(false);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                t.restart();
            }
        });
        add(video, new Integer(3));
        add(stats, new Integer(2));
        if (orientation == SwingConstants.WEST) {
            layout.putConstraint(SpringLayout.WEST, stats, 0, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.SOUTH, stats, 0, SpringLayout.SOUTH, this);
            layout.putConstraint(SpringLayout.EAST, video, 0, SpringLayout.EAST, this);
            //layout.putConstraint(SpringLayout.WEST, video, 0, SpringLayout.EAST, stats);
        } else if (orientation == SwingConstants.EAST) {
            layout.putConstraint(SpringLayout.EAST, stats, 0, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.SOUTH, stats, 0, SpringLayout.SOUTH, this);
            //layout.putConstraint(SpringLayout.EAST, video, 0, SpringLayout.WEST, stats);
            layout.putConstraint(SpringLayout.WEST, video, 0, SpringLayout.WEST, this);
        }
        layout.putConstraint(SpringLayout.SOUTH, video, 0, SpringLayout.SOUTH, this);
        //layout.putConstraint(SpringLayout.NORTH, video, 0, SpringLayout.NORTH, this);

        //setBorder(BorderFactory.createLineBorder(Color.GREEN));
        //cardback.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        //stats.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        //video.setBackground(Color.LIGHT_GRAY);
        video.setOpaque(false);

        betPanel = new BetPanel();
        layout.putConstraint(SpringLayout.NORTH, betPanel, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, betPanel, -5, SpringLayout.EAST, this);
    }

    public void setVideoLabelImage(BufferedImage img) {
        try {
            videoLabel.setIcon(new ImageIcon(Thumbnails.of(img).size(video.getPreferredSize().width, video.getPreferredSize().height).asBufferedImage()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public boolean hasPicture(){
        return (videoLabel.getIcon()!=null);
    }
    public void hideCards() {
        if (cardback != null) {
            cardback.setVisible(false);
        } else if (playerCards != null) {
            playerCards.setVisible(false);
        }
    }

    public void showCardback() {
        if (cardback != null) {
            cardback.setVisible(true);
        }
    }

    public int getBalance() {
        return Integer.valueOf(stats.getBalance().getText().substring(1));
    }
    public void setBalance(int newBalance){
        stats.getBalance().setText(String.format("$%d", newBalance));
    }
    public void increaseBalance(int increasement){
        stats.getBalance().setText(String.format("$%d", (increasement+getBalance())));
    }
    public void decreaseBalance(int decreasement){
        stats.getBalance().setText(String.format("$%d", (getBalance()-decreasement)));
    }

    public void showActiveCards(CardLabel... cards) {
        if (playerCards != null) {
            playerCards.clearCards();
            playerCards.putCards(cards);
            playerCards.setVisible(true);
            playerCards.updateIconsPref(stats.getWidth() + 20, stats.getHeight() + 10);
            revalidate();
        }
    }

    public StatsPanel getStats() {
        return stats;
    }

    public void setStatsIcon(ImageIcon icon) {
        stats.background.setIcon(icon);
        revalidate();
    }

    public void setCardIcon(ImageIcon icon) {
        if (cardback != null) {
            cardback.setIcon(icon);
            revalidate();
        }
    }

    public void updateChipIcons(String chipsFolderPath) {
        dealerLabel.setIcon(new ImageIcon(getClass().getResource(chipsFolderPath + "chip-d.png")));
        revalidate();
    }

    public void setDealer(boolean isDealer) {
        dealerLabel.setVisible(isDealer);
        repaint();
    }

    public class StatsPanel extends JLayeredPane {
        private JTextField nickname;
        private JTextField balance;
        private JLabel background = new JLabel();

        public StatsPanel(String _nickname, String _balance) {
            super();
            setNickname(new JTextField(_nickname));
            getNickname().setHorizontalAlignment(SwingConstants.CENTER);
            getNickname().setForeground(Color.WHITE);
            getNickname().setBorder(null);
            getNickname().setOpaque(false);
            getNickname().setEditable(false);
            getNickname().setFocusable(false);
            setBalance(new JTextField("$" + _balance));
            getBalance().setHorizontalAlignment(SwingConstants.CENTER);
            getBalance().setForeground(Color.WHITE);
            getBalance().setBorder(null);
            getBalance().setOpaque(false);
            getBalance().setEditable(false);
            getBalance().setFocusable(false);
            getBalance().getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    StatsPanel.this.revalidate();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    StatsPanel.this.revalidate();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    StatsPanel.this.revalidate();
                }
            });
            add(getNickname(), new Integer(1));
            add(getBalance(), new Integer(1));
            add(background, new Integer(0));
            SpringLayout layout = new SpringLayout();
            layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, balance, 0, SpringLayout.HORIZONTAL_CENTER, this);
            setLayout(layout);
            layout.putConstraint(SpringLayout.SOUTH, StatsPanel.this, 0, SpringLayout.SOUTH, background);
            layout.putConstraint(SpringLayout.EAST, StatsPanel.this, 0, SpringLayout.EAST, background);
            getBalance().setForeground(new Color(0, 150, 0));
            background.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    Dimension cur = ((TablePanel) PlayerPanel.this.getParent()).getCurrentDim();
                    if (cur == d0) {
                        getNickname().setFont(new Font("Tahoma", Font.PLAIN, 10));
                        getBalance().setFont(new Font("Tahoma", Font.BOLD, 10));
                    } else if (cur == d1) {
                        getNickname().setFont(new Font("Tahoma", Font.PLAIN, 12));
                        getBalance().setFont(new Font("Tahoma", Font.BOLD, 12));
                    } else if (cur == d2) {
                        getNickname().setFont(new Font("Tahoma", Font.PLAIN, 14));
                        getBalance().setFont(new Font("Tahoma", Font.BOLD, 14));
                    } else if (cur == d3) {
                        getNickname().setFont(new Font("Tahoma", Font.PLAIN, 16));
                        getBalance().setFont(new Font("Tahoma", Font.BOLD, 16));
                    } else if (cur == d4) {
                        getNickname().setFont(new Font("Tahoma", Font.PLAIN, 18));
                        getBalance().setFont(new Font("Tahoma", Font.BOLD, 18));
                    } else if (cur == d5) {
                        getNickname().setFont(new Font("Tahoma", Font.PLAIN, 20));
                        getBalance().setFont(new Font("Tahoma", Font.BOLD, 20));
                    } else if (cur == d6) {
                        getNickname().setFont(new Font("Tahoma", Font.PLAIN, 22));
                        getBalance().setFont(new Font("Tahoma", Font.BOLD, 22));
                    }
                    FontMetrics nfm = getNickname().getFontMetrics(getNickname().getFont());
                    FontMetrics bfm = getBalance().getFontMetrics(getBalance().getFont());
                    layout.putConstraint(SpringLayout.NORTH, getNickname(), background.getHeight() / 2 - nfm.getHeight(), SpringLayout.NORTH, background);
                    layout.putConstraint(SpringLayout.WEST, getNickname(), background.getWidth() / 2 - nfm.stringWidth(getNickname().getText()) / 2, SpringLayout.WEST, background);
                    layout.putConstraint(SpringLayout.NORTH, getBalance(), background.getHeight() / 2, SpringLayout.NORTH, background);
                    revalidate();
                }
            });
        }

        public JTextField getNickname() {
            return nickname;
        }

        public void setNickname(JTextField nickname) {
            this.nickname = nickname;
        }

        public JTextField getBalance() {
            return balance;
        }

        public void setBalance(JTextField balance) {
            this.balance = balance;
        }
    }
}
