package table;

import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class BoardCardsPanel extends JPanel implements Constants {
    private List<CardLabel> cards = new LinkedList<>();
    private String iconsPath = null;
    private boolean needUpdate = false;

    public BoardCardsPanel() {
        setOpaque(false);
        setLayout(new FlowLayout());
    }

    public void putCards(CardLabel... cardLabels) {
        for (CardLabel cl : cardLabels) {
            cards.add(cl);
            add(cl);
        }
    }

    public void clearCards() {
        this.removeAll();
        cards.clear();
    }

    public void setIconPath(String path) {
        if (!path.equals(iconsPath)) {
            iconsPath = path;
            needUpdate = true;
        }
    }

    public void updateIcons() {
        if (needUpdate && (iconsPath != null)) {
            try {
                BufferedImage cardImage = ImageIO.read(getClass().getResource(iconsPath));
                int iconW = cardImage.getWidth() / 13;
                int iconH = cardImage.getHeight() / 4;
                int x = 0, y = 0;
                for (CardLabel c : cards) {
                    x = c.getValue() - 2;
                    switch (c.getSuit()) {
                        case HEARTS:
                            y = 0;
                            break;
                        case DIAMONDS:
                            y = 1;
                            break;
                        case CLUBS:
                            y = 2;
                            break;
                        case SPADES:
                            y = 3;
                            break;
                    }
                    c.setIcon(new ImageIcon(cardImage.getSubimage(x * iconW, y * iconH, iconW, iconH)));
                }
                needUpdate = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void setNeedUpdate(boolean needUpdate){
        this.needUpdate = needUpdate;
    }
    public void updateIconsPref(int w, int h) {
        try {
            BufferedImage cardImage = ImageIO.read(getClass().getResource(iconsPath));
            int iconW = cardImage.getWidth() / 13;
            int iconH = cardImage.getHeight() / 4;
            int x = 0, y = 0;
            for (CardLabel c : cards) {
                x = c.getValue() - 2;
                switch (c.getSuit()) {
                    case HEARTS:
                        y = 0;
                        break;
                    case DIAMONDS:
                        y = 1;
                        break;
                    case CLUBS:
                        y = 2;
                        break;
                    case SPADES:
                        y = 3;
                        break;
                }
                c.setIcon(new ImageIcon(Thumbnails.of(cardImage.getSubimage(x * iconW, y * iconH, iconW, iconH)).size(w, h).asBufferedImage()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
