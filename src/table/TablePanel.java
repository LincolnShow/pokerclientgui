package table;

import com.github.sarxos.webcam.Webcam;
import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;


public class TablePanel extends JLayeredPane implements Constants {
    private final static BufferedImage backgroundImage;
    private JLabel backgroundLabel = new JLabel();
    private BoardCardsPanel boardCards = new BoardCardsPanel();
    private BetPanel pot = new BetPanel();
    private Timer timer;
    private PlayerPanel[] players = new PlayerPanel[6];
    private BetPanel[] betPanels = new BetPanel[6];
    private SpringLayout layout = new SpringLayout();
    private static Dimension currentDim;
    private byte currentDimStep = 0;
    private Dimension oldDim = new Dimension(1, 1);

    static {
        BufferedImage background = null;
        try {
            background = ImageIO.read(TablePanel.class.getResource("game-background.jpg"));
        } catch (IOException e) {
        }
        backgroundImage = background;
    }

    public TablePanel() {
        super();
        setLayout(layout);
        add(backgroundLabel, new Integer(1));
        add(getBoardCards(), new Integer(4));
        add(pot, new Integer(4));
        pot.setVisible(false);
        pot.getBetValue().setForeground(Color.YELLOW);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, pot, 0, SpringLayout.HORIZONTAL_CENTER, this);
        layout.putConstraint(SpringLayout.NORTH, pot, 0, SpringLayout.SOUTH, getBoardCards());
        Timer t1 = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.putConstraint(SpringLayout.NORTH, getBoardCards(), (getHeight() / 2) - (getBoardCards().getHeight() / 2), SpringLayout.NORTH, TablePanel.this);
                layout.putConstraint(SpringLayout.WEST, getBoardCards(), (getWidth() / 2) - (getBoardCards().getWidth() / 2), SpringLayout.WEST, TablePanel.this);
                revalidate();
            }
        });
        t1.setRepeats(false);
        getBoardCards().addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                t1.restart();
            }
        });
        /*boardCards.putCards(new CardLabel(14, CardInformation.CardSuit.CLUBS));
        boardCards.putCards(new CardLabel(13, CardInformation.CardSuit.SPADES));
        boardCards.putCards(new CardLabel(12, CardInformation.CardSuit.HEARTS));
        boardCards.putCards(new CardLabel(11, CardInformation.CardSuit.DIAMONDS));
        boardCards.putCards(new CardLabel(10, CardInformation.CardSuit.CLUBS));*/
        timer = new Timer(50, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Dimension r = getSize();
                try {
                    backgroundLabel.setIcon(new ImageIcon(Thumbnails.of(backgroundImage).size(r.width, r.height).keepAspectRatio(false).asBufferedImage()));
                    validateIcons(TablePanel.this.getParent().getSize(), false);
                    layout.putConstraint(SpringLayout.NORTH, getBoardCards(), (getHeight() / 2) - (getBoardCards().getHeight() / 2) - 7, SpringLayout.NORTH, TablePanel.this);
                    layout.putConstraint(SpringLayout.WEST, getBoardCards(), (getWidth() / 2) - (getBoardCards().getWidth() / 2), SpringLayout.WEST, TablePanel.this);
                    int newH = (int) ((160.0 / 176.0) * r.height / 4f);
                    pot.setPreferredSize(new Dimension((int) ((r.width/4) / 2.5), (int) (newH / 1.3)));
                    for (int position = 0; position < 6; ++position) {
                        if (getPlayers()[position] != null) {
                            if (position == 0 || position == 4) { // LEFT
                                getPlayers()[position].setPreferredSize(new Dimension(r.width / 4, newH));
                            } else if (position == 1 || position == 3) { //RIGHT
                                getPlayers()[position].setPreferredSize(new Dimension(r.width / 4, newH));
                            } else if (position == 2 || position == 5) { //DOWN
                                getPlayers()[position].setPreferredSize(new Dimension(r.width / 4, newH));
                            }
                            switch (position) {
                                case (0):
                                    layout.putConstraint(SpringLayout.NORTH, getPlayers()[position], r.height / 20, SpringLayout.NORTH, TablePanel.this);
                                    layout.putConstraint(SpringLayout.EAST, getPlayers()[position], r.width / 2 - 20, SpringLayout.WEST, TablePanel.this);
                                    break;
                                case (1):
                                    layout.putConstraint(SpringLayout.NORTH, getPlayers()[position], r.height / 20, SpringLayout.NORTH, TablePanel.this);
                                    layout.putConstraint(SpringLayout.WEST, getPlayers()[position], (r.width / 2 + 20), SpringLayout.WEST, TablePanel.this);
                                    break;
                                case (2):
                                    layout.putConstraint(SpringLayout.NORTH, getPlayers()[position], r.height / 2 - newH / 2, SpringLayout.NORTH, TablePanel.this);
                                    layout.putConstraint(SpringLayout.EAST, getPlayers()[position], -5, SpringLayout.EAST, TablePanel.this);
                                    break;
                                case (3):
                                    layout.putConstraint(SpringLayout.SOUTH, getPlayers()[position], -r.height / 20, SpringLayout.SOUTH, TablePanel.this);
                                    layout.putConstraint(SpringLayout.WEST, getPlayers()[position], (r.width / 2 + 20), SpringLayout.WEST, TablePanel.this);
                                    break;
                                case (4):
                                    layout.putConstraint(SpringLayout.SOUTH, getPlayers()[position], -r.height / 20, SpringLayout.SOUTH, TablePanel.this);
                                    layout.putConstraint(SpringLayout.EAST, getPlayers()[position], r.width / 2 - 20, SpringLayout.WEST, TablePanel.this);
                                    break;
                                case (5):
                                    layout.putConstraint(SpringLayout.NORTH, getPlayers()[position], r.height / 2 - newH / 2, SpringLayout.NORTH, TablePanel.this);
                                    layout.putConstraint(SpringLayout.WEST, getPlayers()[position], 5, SpringLayout.WEST, TablePanel.this);
                                    break;
                            }
                            if (getBetPanels()[position] != null) {
                                getBetPanels()[position].setPreferredSize(new Dimension((int) (getPlayers()[position].getWidth() / 3), (int) (getPlayers()[position].getHeight() / 1.7)));
                            }
                        }
                    }
                } catch (IOException ignored) {
                }
                revalidate();
            }
        });
        timer.setRepeats(true);
        timer.start();
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                if (!timer.isRunning()) {
                    timer.restart();
                }
            }
        });
    }

    public static Dimension getCurrentDim() {
        return currentDim;
    }

    public static void setCurrentDim(Dimension currentDim) {
        TablePanel.currentDim = currentDim;
    }

    public void setPotValue(int value){
        pot.getBetValue().setText(String.format("$%d", value));
    }
    public void increasePot(int increasement){
        pot.getBetValue().setText(String.format("$%d", pot.getBet()+increasement));
    }
    public void decreasePot(int decreasement){
        pot.getBetValue().setText(String.format("$%d", pot.getBet()-decreasement));
    }
    public void hideAndClearPot(){
        pot.getBetValue().setText("$0");
        pot.setVisible(false);
    }
    public void showPot(){
        pot.setVisible(true);
    }
    public void validateIcons(Dimension newResolution, boolean forced) {
        int newW = newResolution.width, newH = newResolution.height;
        String statsIconPath = null;
        String cardbackIconPath = null;
        String boardCardsIconPath = null;
        if (newW >= d6.width && newH >= d6.height) {
            setCurrentDim(d6);
            currentDimStep = 6;
            statsIconPath = "5/pp.png";
            cardbackIconPath = "3/cardback.png";
            boardCardsIconPath = "6/cards.png";
        } else if (newW >= d5.width && newH >= d5.height) {
            setCurrentDim(d5);
            currentDimStep = 5;
            statsIconPath = "5/pp.png";
            cardbackIconPath = "2/cardback.png";
            boardCardsIconPath = "5/cards.png";
        } else if (newW >= d4.width && newH >= d4.height) {
            setCurrentDim(d4);
            currentDimStep = 4;
            statsIconPath = "4/pp.png";
            cardbackIconPath = "2/cardback.png";
            boardCardsIconPath = "4/cards.png";
        } else if (newW >= d3.width && newH >= d3.height) {
            setCurrentDim(d3);
            currentDimStep = 3;
            statsIconPath = "3/pp.png";
            cardbackIconPath = "1/cardback.png";
            boardCardsIconPath = "3/cards.png";
        } else if (newW >= d2.width && newH >= d2.height) {
            setCurrentDim(d2);
            currentDimStep = 2;
            statsIconPath = "2/pp.png";
            cardbackIconPath = "1/cardback.png";
            boardCardsIconPath = "2/cards.png";
        } else if (newW >= d1.width && newH >= d1.height) {
            setCurrentDim(d1);
            currentDimStep = 1;
            statsIconPath = "1/pp.png";
            cardbackIconPath = "0/cardback.png";
            boardCardsIconPath = "1/cards.png";
        } else {
            setCurrentDim(d0);
            currentDimStep = 0;
            statsIconPath = "0/pp.png";
            cardbackIconPath = "0/cardback.png";
            boardCardsIconPath = "0/cards.png";
        }
        if (forced || (getCurrentDim() != oldDim)) {
            ImageIcon statsIcon = new ImageIcon(getClass().getResource(statsIconPath));
            ImageIcon cbIcon = new ImageIcon(getClass().getResource(cardbackIconPath));
            for (PlayerPanel pp : getPlayers()) {
                if (pp != null) {
                    pp.setStatsIcon(statsIcon);
                    pp.setCardIcon(cbIcon);
                    pp.updateChipIcons(String.valueOf(currentDimStep == 0 ? 0 : currentDimStep - 1) + "/chips/");
                }
            }
            getBoardCards().setIconPath(boardCardsIconPath);
            getBoardCards().updateIcons();
            /*System.out.println(statsIconPath);
            System.out.println(cardbackIconPath);
            System.out.println(currentDim);*/
        }
        oldDim = getCurrentDim();
        revalidate();
    }

    public void putPlayer(String name, int balance, int position, Webcam activeWebcam, boolean active) {
        PlayerPanel pp = null;
        if (position == 5) {
            pp = new PlayerPanel(name, balance, SwingConstants.WEST, activeWebcam, active);
        } else {
            pp = new PlayerPanel(name, balance, SwingConstants.EAST, activeWebcam, active);
        }
        getPlayers()[position] = pp;
        add(getPlayers()[position], new Integer(3));
        timer.restart();
        /*while(!timer.isRunning()){}
        while(timer.isRunning()){}
        revalidate();
        pp.revalidate();
        pp.getStats().revalidate();*/
    }

    public void makePlayerBet(int position, int betValue) {
        if (getBetPanels()[position] == null) {
            getBetPanels()[position] = new BetPanel();
            add(getBetPanels()[position], new Integer(3));
            if (position == 0) {
                layout.putConstraint(SpringLayout.NORTH, getBetPanels()[position], 10, SpringLayout.SOUTH, getPlayers()[position]);
                layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, getBetPanels()[position], 15, SpringLayout.HORIZONTAL_CENTER, getPlayers()[position]);
            } else if (position == 1) {
                layout.putConstraint(SpringLayout.NORTH, getBetPanels()[position], 10, SpringLayout.SOUTH, getPlayers()[position]);
                layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, getBetPanels()[position], -15, SpringLayout.HORIZONTAL_CENTER, getPlayers()[position]);
            } else if (position == 2) {
                layout.putConstraint(SpringLayout.SOUTH, getBetPanels()[position], 0, SpringLayout.SOUTH, getPlayers()[position]);
                layout.putConstraint(SpringLayout.EAST, getBetPanels()[position], 10, SpringLayout.WEST, getPlayers()[position]);
            } else if (position == 3) {
                layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, getBetPanels()[position], -15, SpringLayout.HORIZONTAL_CENTER, getPlayers()[position]);
                layout.putConstraint(SpringLayout.SOUTH, getBetPanels()[position], -5, SpringLayout.NORTH, getPlayers()[position]);
            } else if (position == 4) {
                layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, getBetPanels()[position], 15, SpringLayout.HORIZONTAL_CENTER, getPlayers()[position]);
                layout.putConstraint(SpringLayout.SOUTH, getBetPanels()[position], -5, SpringLayout.NORTH, getPlayers()[position]);
            } else if (position == 5) {
                layout.putConstraint(SpringLayout.SOUTH, getBetPanels()[position], 0, SpringLayout.SOUTH, getPlayers()[position]);
                layout.putConstraint(SpringLayout.WEST, getBetPanels()[position], -10, SpringLayout.EAST, getPlayers()[position]);
            }
        }
        getBetPanels()[position].getBetValue().setText("$" + String.valueOf(betValue));
        getBetPanels()[position].setVisible(true);
    }

    public void removeBet(int position) {
        getBetPanels()[position].setVisible(false);
        getBetPanels()[position].getBetValue().setText("$0");
        //layout.removeLayoutComponent(betPanels[position]);
        //betPanels[position] = null;
    }
    public void changeBet(int position, int newBet){
        getBetPanels()[position].getBetValue().setText("$" + newBet);
        getBetPanels()[position].setVisible(true);
    }
    public void increaseBet(int position, int addition){
        getBetPanels()[position].getBetValue().setText("$" + String.valueOf(Integer.valueOf(getBetPanels()[position].getBetValue().getText().substring(1))+addition));
        getBetPanels()[position].setVisible(true);
    }

    public void clearBets() {
        for (int i = 0; i < 6; ++i) {
            if(betPanels[i] != null) {
                removeBet(i);
            }
        }
    }

    public void removePlayer(int position) {
        remove(getPlayers()[position]);
        getPlayers()[position] = null;
    }

    public void setDealer(int position) {
        for (PlayerPanel pp : getPlayers()) {
            if (pp != null) {
                pp.setDealer(false);
            }
        }
        getPlayers()[position].setDealer(true);
    }
    public void putBoardCards(CardLabel ... cls){
        getBoardCards().putCards(cls);
    }
    public void hideAllCards() {
        getBoardCards().clearCards();
        for (PlayerPanel p : getPlayers()) {
            if (p != null) {
                p.hideCards();
            }
        }
        for (BetPanel bp : getBetPanels()) {
            if (bp != null) {
                bp = null;
            }
        }
    }

    public BoardCardsPanel getBoardCards() {
        return boardCards;
    }

    public PlayerPanel[] getPlayers() {
        return players;
    }

    public BetPanel[] getBetPanels() {
        return betPanels;
    }
}
