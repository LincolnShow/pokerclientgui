package table;

import javax.swing.*;

public class CardLabel extends JLabel implements CardInformation {
    private int value = -1;
    private CardSuit suit = null;

    public CardLabel(int value, CardSuit suit) {
        if (value < 2 || value > 14) {
            throw new RuntimeException("value must be in the range of 2-14");
        }
        this.value = value;
        this.suit = suit;
    }

    public int getValue() {
        return value;
    }

    public CardSuit getSuit() {
        return suit;
    }
}
