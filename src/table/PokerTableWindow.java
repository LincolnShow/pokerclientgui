package table;

import javafx.application.Platform;
import server.RequestHandler;
import server.Worker;
import server.requests.GetPlayerInfoRequest;
import server.requests.LeaveTableRequest;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PokerTableWindow implements Constants {
    private Timer timer;
    private JFrame frame;
    private BottomPanel bottomPanel;
    private TablePanel tablePanel;
    private JPanel panel;
    private static int i = 0;

    public PokerTableWindow() {
        setFrame(new JFrame("Window"));
        getFrame().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        getFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                RequestHandler.send(new LeaveTableRequest(Worker.pp.getName()));
                Platform.runLater(() -> {
                    Worker.isSitting = false;
                    Worker.updateLobby = true;
                    if(Worker.vIt != null){
                        Worker.vIt.interrupt();
                        Worker.vIt = null;
                    }
                    if(Worker.vOt != null){
                        Worker.vOt.interrupt();
                        Worker.vOt = null;
                    }
                    for(int i : Worker.activePorts){
                        i = 0;
                    }
                    if(Worker.vOt != null) {
                        Worker.vOt.interrupt();
                        Worker.vOt = null;
                    }
                    if(Worker.vIt != null) {
                        Worker.vIt.interrupt();
                        Worker.vIt = null;
                    }
                    Worker.pGame = null;
                    Worker.primaryStage.show();
                });
            }
        });
        setBottomPanel(new BottomPanel());
        getBottomPanel().setVisible(true);
        setTablePanel(new TablePanel());
        getTablePanel().setVisible(true);
        panel = new JPanel(new BorderLayout());
        getFrame().add(panel);
        panel.add(getBottomPanel(), BorderLayout.SOUTH);
        panel.add(getTablePanel(), BorderLayout.CENTER);
        panel.setPreferredSize(d1);
        panel.setMinimumSize(panel.getPreferredSize());
        getFrame().pack();
        getFrame().setMinimumSize(getFrame().getSize());
        /*timer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getFrame().setTitle("WINDOW: " + getFrame().getWidth() + "-" + getFrame().getHeight() + "          FRAME: " + getFrame().getContentPane().getWidth() + "-" + getFrame().getContentPane().getHeight());
            }
        });
        timer.setRepeats(false);
        timer.start();
        getFrame().addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                timer.restart();
            }
        });*/
        getFrame().setTitle(Worker.tip.getTableName());
        getFrame().setLocationRelativeTo(null);
        if (Worker.pGame == null) {
            Worker.pGame = this;
        }
        getBottomPanel().getActions().getActionb1().addActionListener(e -> {
            Worker.pGame.getTablePanel().putPlayer("Unknown", 50000, i++, null, false);
            Worker.pGame.getTablePanel().validateIcons(getFrame().getSize(), true);
        });
        getBottomPanel().getActions().getActionb2().addActionListener(e -> {
            Worker.pGame.getTablePanel().makePlayerBet(i - 1, 100);
        });
        getBottomPanel().getActions().getExitButton().addActionListener(e -> {
            getFrame().dispatchEvent(new WindowEvent(getFrame(), WindowEvent.WINDOW_CLOSING));
        });
    }

    public void setVisible(boolean isVisible) {
        getFrame().setVisible(isVisible);
    }

    public void setFullscreen() {
        getFrame().setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public BottomPanel getBottomPanel() {
        return bottomPanel;
    }

    public void setBottomPanel(BottomPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }

    public TablePanel getTablePanel() {
        return tablePanel;
    }

    public void setTablePanel(TablePanel tablePanel) {
        this.tablePanel = tablePanel;
    }
}
