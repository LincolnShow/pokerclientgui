package test;

import net.coobird.thumbnailator.Thumbnails;
import server.requests.GetPocketCardsRequest;
import table.CardLabel;
import table.PlayerPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Socket;

class Main {
    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        SpringLayout sl = new SpringLayout();
        PlayerPanel pp = new PlayerPanel("Player", 1200, SwingConstants.EAST, null, false);
        frame.add(pp);
        frame.pack();
        frame.setVisible(true);
        BufferedImage img = ImageIO.read(new File("D:/face.png"));

        Timer t = new Timer(1, e -> {
            try {
                pp.setVideoLabelImage(Thumbnails.of(img).size(176, 144).keepAspectRatio(false).asBufferedImage());
                frame.revalidate();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        t.setRepeats(false);
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                t.restart();
            }
        });
    }
}
